/********************************************************************
* Script: Live games
* Description: Continuously fetches newest game scores from database
* Date: 12.4.2014
********************************************************************/

var UPDATE_FREQUENCY = 5000;

$(document).ready(function()
{
	updateLiveGames();
	var loop = setInterval(updateLiveGames, UPDATE_FREQUENCY);
});

function updateLiveGames()
{
	$.ajax({
			type: "POST",
			url: "../view/fetch_live_games.php"
	})
	
	.success(function(msg) 
	{
		$("#livegames").html(msg);
	});
}