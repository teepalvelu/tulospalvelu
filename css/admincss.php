<?php 
	require_once('../includes/settingsparser.php');
	$s = new SettingsParser;
	
	$css = $s->ini['CSS_ADMIN'];
?>

<style>
	body
	{
		<?php
			if($css['css_admin_body_background_selection'] == "color")
			{
				echo "background-color: " . $css['css_admin_body_background'] . ";\n";
			}
			else
			{
				echo "background: url('" . getBasePath() . "/../images/" .  $css['css_admin_body_background_image'] . "') repeat;\n";
			}
		?>
		font-family: 'Exo 2', sans-serif;
		font-weight: 300;
		color: <?php echo $css['css_admin_body_color']; ?>;
		width: 100%;
		height: 100%;
	}

	.table
	{
		width: auto;
		position: relative;
	}

	.btn{
		margin: 2px;
	}

	.table > thead > tr > th, 
	.table > tbody > tr > th, 
	.table > tfoot > tr > th, 
	.table > thead > tr > td, 
	.table > tbody > tr > td, 
	.table > tfoot > tr > td{
		padding: 4px;
	}

	.table.table.table-bordered-games td{
	font-size: 13px;
	padding: 0px;
	vertical-align: middle;
	border: 1px solid #dddddd;
	}

	form{
	margin: 0px;
	}


	#container
	{
		background-color: <?php echo $css['css_admin_container_background']; ?>;
		margin: 0 auto 0 auto;
		width: 800px;
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		margin-top: 10px;	
		height: 650px;
	}

	#header
	{
		text-align: center;
		background-color: <?php echo $css['css_admin_header_background']; ?>;
		border-radius: 10px 10px 0px 0px;
		-moz-border-radius: 10px 10px 0px 0px;
		-webkit-border-radius: 10px 10px 0px 0px;
		height: 100px;
	}

	h1.header
	{
		line-height: 100px;
		color: black;
	}

	h2
	{
		margin-top: 0px;
	}

	#content
	{
		color: <?php echo $css['css_admin_content_color']; ?>;
		background-color: <?php echo $css['css_admin_content_background']; ?>;
		height: 100%;
		width: 100%;
		padding: 5px 5px 15px 5px;
		margin-top: 0px;
		display: table;
	}

	#col-left
	{
		width: 200px;
		float: left;
		overflow: hidden;
		padding-right: 5px;
		border-right: 1px solid black;	
	}

	#col-right
	{
		width: 590px;
		padding-left: 5px;
		float: left;
	}

	#footer
	{
		text-align: center;
		background-color: <?php echo $css['css_admin_footer_background']; ?>;
		border-radius: 0px 0px 5px 5px;
		-moz-border-radius: 0px 0px 5px 5px;
		-webkit-border-radius: 0px 0px 5px 5px;
		height: 50px;
		padding: 1px;
		margin-top: 0px;
	}

	h1.footer
	{
		line-height: 50px;
		margin-top: 0px;
	}

	#error
	{
		background-color: <?php echo $css['css_admin_errors_background']; ?>;
		color: <?php echo $css['css_admin_errors_color']; ?>;
		font-weight: bold;
		text-align: center;
		margin: 5px 0 5px 0;
		border: 2px dotted red;
	}

	#error, p
	{
		padding: 3px 0 3px 0;
	}

	p.left, p.right
	{
		line-height: 50px;
		padding: 0;
	}

	p.left
	{
		float: left;
		margin-left: 10px;
	}

	p.right
	{
		float: right;
		margin-right: 10px;
	}
</style>