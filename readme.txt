﻿**********************************************************
fi.jamk.iio12s1.tulospalvelu

PHP, tietokantojen ja www-tekniikoiden harjoitustyö.
JAMK 2014

**********************************************************

Tarkoituksena on kehittää pieniin urheiluturnauksiin soveltuva 
webbipohjainen tulospalvelu.

************************TO-DO*****************************
USER:
	* Ottelulistaan osatulokset ja järjestelyvuorot
		
	* Sarjataulukon pisteiden laskeminen otteluiden lopputulosten perusteella lohkossa

	* Sarjataulukkoon tehdyt vs päästetyt sarakkeet
		[Joukkue] [Voitot] [Tasa] [Tappio] [Saadut] [Päästetyt] [Piseet] 
		
	* Turnauksen uusimmat tuloset
	
	* Tällä hetkellä käynnissä olevat ottelut

ADMIN:
	* Login käyttäjänimellä ja salasanalla
		* Salasana unohtunut? -toiminnallisuus
	
	* Turnauskohtaiset käyttöoikeudet
	
	* Pääkäyttäjälle mahdollisuus hallita kaikkia turnauksia
		* mahdollisuus luoda uusia turnauksia ja poistaa/piilottaa niitä
		* oikeus kaikkiin toimenpiteisiin
	
	* Turnauksen asetukset
		* Nimi, aikaväli (alku - loppu), paikkakunta, uusien lohkoasetusten oletukset
			sarjataulukot järjestys asetus (1. pisteet, 2. tehdyt, 3. päästetyt, jne.),
			tulossivun teeman valinta
	
	* Sarjan lisäys turnaukseen
	
	* Lohkon lisäys sarjaan
		* Lohkon asetukset: Voitto, tasa, häviöpisteet, ottelutyyppi (välitulosten määrä 1-5)
			näytetäänkö sarjataulukko julkisesti
	
	* Joukkueen lisäys lohkoon
	
	* Otteluiden luominen
		* vasta kun lohkossa on kaksi joukkuetta (no daa)
		* Valitaan joukkueet
		* Valitaan päivä ja kellonaika
		* Valitaan kenttä
		* Järjestelyvuoro (dropdown lista turnauksen joukkueista)
		
	* Otteluiden muokkaaminen
	
	* Turnausraportit
		* Ottelut kentittäin
		* Kaikki ottelut
		* Ottelut joukkueettain
		
	* Turnauksen pöytäkirjojen tulostus pdf
	
	