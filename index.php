<?php
/*index.php*/
require_once('includes/startup.php');
require_once('includes/functions.php');
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Tulospalvelu</title>
		<meta charset="UTF-8">
		<link href='https://fonts.googleapis.com/css?family=Exo+2:400,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo getBasePath();?>/scripts/dist/css/bootstrap.css">
		<!--<link rel="stylesheet" type="text/css" href="<?php echo getBasePath();?>/css/styles.css">-->
		<?php include('css/css.php'); ?>
		<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src = "<?php echo getBasePath();?>/scripts/dist/js/bootstrap.js"></script>
		
		<?php
			$c = getCommands();
			if($c[0] == "" || $c[0] == "tournamentlist")
			{
				echo "<script src = '" . getBasePath() . "/scripts/live_games.js'></script>";	
			}
		?>
				
	</head>
	
	<body>
		<div id="container">				
			<div id="header">
				<h1 class = "header"><?php echo '<a href="'.getBasePath().'/tournamentlist/">'; ?>Tulospalvelu</a></h1>
			</div>
			
			<div id="content">		
				<ol class="breadcrumb">
				<li><a href = "<?php echo getBasePath().'/tournamentlist/'; ?>">Tulospalvelu</a></li>
					<?php
					$c = getCommands();
					@$crumbarray = resolver($c[0],$c[1]);
					if (is_array($crumbarray)){
						$last = end($crumbarray);
						foreach($crumbarray as $key => $step){
							if($step == $last){
								echo '<li class="active">'.$step["name"].'</li>';}
							else{
								echo '<li><a href="'.getBasePath().'/'.$key.'/'.$step["id"].'">'.$step["name"].'</a></li>';
							}
						}
					}
					?>
				</ol>
				
				
				
				<?php if(count(Errors::getErrorlist())) { ?>
				
				<div class="alert alert-dismissable alert-danger">
					<?php parseErrors(Errors::getErrorlist()); ?>
				</div>
				
				<?php } ?>
				
				<?php 
					showContent();
				?>
				
				<div id = "livegames"></div>
			</div>
			
			<div id="footer">
				<!--<h1 class = "footer">Le' alapalkki</h1>-->
				<p class = "left">Le' alapalkki</p>
				<p class = "right"><a href = "<?php echo getBasePath();?>/admin/">Admin panel</a></p>
			</div>	
		</div>
	</body>
</html>