<?php
abstract class Errors
{
    static private $errorlist = array(); 

    public static function getErrorlist()
    {	
        return self::$errorlist;
    }

    public static function setError($message)
    {
        array_push(self::$errorlist, $message);
    }
}
?>