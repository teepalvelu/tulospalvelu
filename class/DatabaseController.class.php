<?php
	/********************************************************
	* Script: Database controller
	* Description: Connects and fetches data from database
	********************************************************/

	class DatabaseController 
	{
		private $_database;
		private $_db_user;
		private $_db_pass;
		private $_db_host;
		private $_db_name;
		
		function __construct() 
		{
			$this->_db_user = Settings::getProtected('db_username');
			$this->_db_pass = Settings::getProtected('db_password');
			$this->_db_host = Settings::getProtected('db_hostname');
			$this->_db_name = Settings::getProtected('db_database');
			$this->_database = $this->connectDatabase();
		}

		function __destruct() 
		{
			$this->_database = NULL;
		}
		
		//TIETOKANTAAN YHDISTÄMINEN
		function connectDatabase()
		{
			try
			{
				return new PDO('mysql:host='.$this->_db_host.';dbname='.$this->_db_name.';charset=utf8',$this->_db_user, $this->_db_pass);
			}
			catch (PDOException $e)
			{
				Errors::setError("Sorry, there was a database error. We are sorry!");
			}
		}
		
		//TURNAUSLISTAN HAKU
		function getTournaments()
		{
			$query = 'SELECT 
						tournament.tournament_id as t_id,
						tournament.tournament_name as t_name,
						tournament.tournament_time as t_time,
						COUNT(league.league_id) as leagues
					FROM
						tournament
					LEFT OUTER JOIN
						league 
					ON league.tournament_id = tournament.tournament_id
					GROUP BY tournament.tournament_id';
					
			$pre = $this->_database->prepare($query);
			$pre->execute();
			return $pre;
		}
		
		//SARJALISTAN HAKU
		function getLeagues($tid)
		{
			$query = 'SELECT 
						league.league_id as l_id,
						league.league_name as l_name,
						COUNT(division.division_id) as divisions
					FROM
						league
					LEFT OUTER JOIN
						division 
					ON division.league_id = league.league_id
					WHERE league.tournament_id = ?
					GROUP BY league.league_id';
					
			$pre = $this->_database->prepare($query);
			$pre->execute(array($tid));
			return $pre;
		}
		
		//LOHKOLISTAN HAKU
		function getDivisions($did)
		{
			$query = 'SELECT 
						division.division_id as d_id,
						division.division_name as d_name,
						COUNT(team.team_id) as teams
					FROM
						division
					LEFT OUTER JOIN
						team 
					ON team.division_id = division.division_id
					WHERE division.league_id = ?
					GROUP BY division.division_id';
					
			$pre = $this->_database->prepare($query);
			$pre->execute(array($did));
			return $pre;
		}
		

		//TURNAUSNIMEN HAKU
		function tournamentName($tid)
		{
			$query = 'SELECT 
						tournament.tournament_name as tournament_name
					FROM
						tournament
					WHERE tournament.tournament_id = ?';
					
			$pre = $this->_database->prepare($query);
			$pre->execute(array($tid));
			return $pre;
		}
		
		//OTTELUIDEN HAKU
		function getGames($did)
		{
			$query = 'SELECT
					game.game_id as game_id,
					game.game_time as game_time,
					team1.team_name as team1_name,
					team2.team_name as team2_name,
					field.field_name as field_name,
					SUM(score.score_team1) as score_team1,
					SUM(score.score_team2) as score_team2
				FROM
					game AS game
				LEFT JOIN
					team AS team1
					ON team1.team_id = game.game_team1
				LEFT JOIN
					team AS team2
					ON team2.team_id = game.game_team2
				LEFT JOIN
					field AS field
					ON game.field_id = field.field_id
				LEFT JOIN
					score AS score
					ON game.game_id = score.game_id
				WHERE team1.division_id = ? OR team2.division_id = ?
				GROUP BY game.game_id
				ORDER BY game.game_time ASC';
				
			$pre = $this->_database->prepare($query);
			$pre->execute(array($did,$did));
			return $pre;
		}
		//SARJOJEN HAKU
		function getSeries($did) 
		{
			$query = 'SELECT 
						team.team_name as team_name, 
						team.team_win as team_win, 
						team.team_lose as team_lose, 
						team.team_tie as team_tie, 
						((team.team_win * setting.setting_gameWinPt) + (team.team_lose * setting.setting_gameLosePt) + (team.team_tie * setting.setting_gameTiePt)) as team_points 
					FROM 
						team 
					INNER JOIN 
						division 
					ON team.division_id = division.division_id
					INNER JOIN 
						setting 
					ON division.division_setting_id = setting.setting_id
					WHERE division.division_id = ?
					ORDER BY team_points DESC';
					
			$pre = $this->_database->prepare($query);
			$pre->execute(array($did));
			return $pre;
		}
		
		//Divistä ylöispäin olevan datan haku
		function divisionResolver($did) 
		{
			$query = 'SELECT d.division_id, d.division_name, d.league_id, l.league_name, l.tournament_id, t.tournament_name
					FROM division as d LEFT JOIN league as l
					ON l.league_id = d.league_id
					LEFT JOIN tournament as t
					ON l.tournament_id = t.tournament_id
					WHERE division_id = ?;';
					
			$pre = $this->_database->prepare($query);
			$pre->execute(array($did));
			return $pre;
		}
		
		function leagueResolver($lid) 
		{
			$query = 'SELECT l.league_id, l.league_name, l.tournament_id, t.tournament_name
			FROM league as l
			LEFT JOIN tournament as t
			ON l.tournament_id = t.tournament_id
			WHERE league_id = ?;';
					
			$pre = $this->_database->prepare($query);
			$pre->execute(array($lid));
			return $pre;
		}
		
		function tournamentResolver($tid) 
		{
			$query = 'SELECT tournament_id, tournament_name
			FROM
			tournament
			WHERE tournament_id = ?;';
					
			$pre = $this->_database->prepare($query);
			$pre->execute(array($tid));
			return $pre;
		}
		
		function getSettings($id)
		{
			$query = 'SELECT * FROM setting WHERE setting_id = ? LIMIT 1';
			$pre = $this->_database->prepare($query);
			$pre->execute(array($id));
			return $pre;
		}
		
		function getDivisionById($did){
			$query = 'SELECT * FROM division WHERE division_id = ? LIMIT 1';
			$pre = $this->_database->prepare($query);
			$pre->execute(array($did));
			return $pre;
		}
		
		function getScore($gid, $part){
			$query = 'SELECT
				score_team1,
				score_team2,
				score_part
			FROM score
				WHERE game_id = ?
				AND score_part = ?
			LIMIT 1';
	
			$pre = $this->_database->prepare($query);
			$pre->execute(array($gid, $part));
			return $pre;
		}
	}
?>