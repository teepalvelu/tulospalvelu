<h2>Database-settings</h2>
<table class = "installerformtable">
	<tr>
		<td>User:</td>
		<td><input type = "text" name = "db_user" value = "teepalvelu"/></td>
	</tr>
	<tr>
		<td>Password:</td>
		<td><input type = "password" name = "db_password" value = "teepalvelu"/></td>
	</tr>
	<tr>
		<td>Host:</td>
		<td><input type = "text" name = "db_host" value = "teepalvelu.dy.fi"/></td>
	</tr>
	<tr>
		<td>Name:</td>
		<td><input type = "text" name = "db_name" value = "tpalvelu"/></td>
	</tr>
</table>

<h2>Tulospalvelu style-settings</h2>
<table class = "installerformtable">
	<tr>
		<td colspan = "2"><b>BODY</b></td>
	</tr>
	<tr>
		<td>Body background</td>
		<td>
			<?php createColorSelection("css_body_background", "#D7D7BD"); ?>
			Use color: <input type = "radio" name = "css_body_background_selection" value = "color"/>
		</td>
	</tr>
	<tr>
		<td>Body background-image</td>
		<td>
			<?php createImageSelector("css_body_background_image"); ?>
			Use image: <input type = "radio" name = "css_body_background_selection" value = "image" checked = "checked"/>
		</td>
	</tr>
	<tr>
		<td>Body font color</td>
		<td>
			<?php createColorSelection("css_body_color", "#282F36"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>CONTAINER</b></td>
	</tr>
	<tr>
		<td>Container background</td>
		<td>
			<?php createColorSelection("css_container_background", "#1b7e9f"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>HEADER</b></td>
	</tr>
	<tr>
		<td>Header background</td>
		<td>
			<?php createColorSelection("css_header_background", "#D7D7BD"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>CONTENT</b></td>
	</tr>
	<tr>
		<td>Content background</td>
		<td>
			<?php createColorSelection("css_content_background", "#f4f4f4"); ?>
		</td>
	</tr>
	<tr>
		<td>Content font color</td>
		<td>
			<?php createColorSelection("css_content_color", "#282F36"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>FOOTER</b></td>
	</tr>
	<tr>
		<td>Footer background</td>
		<td>
			<?php createColorSelection("css_footer_background", "#D7D7BD"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>ERRORS</b></td>
	</tr>
	<tr>
		<td>Errors background</td>
		<td>
			<?php createColorSelection("css_errors_background", "#FFEDED"); ?>
		</td>
	</tr>
	<tr>
		<td>Errors font color</td>
		<td>
			<?php createColorSelection("css_errors_color", "red"); ?>
		</td>
	</tr>
	<tr>
		<td>Errors font weight</td>
		<td>
			<input type = "text" name = "css_errors_weight" value = "700"/>
		</td>
	</tr>
</table>

<h2>Tulospalvelu admin style-settings</h2>
<table class = "installerformtable">
	<tr>
		<td colspan = "2"><b>BODY</b></td>
	</tr>
	<tr>
		<td>Body background</td>
		<td>
			<?php createColorSelection("css_admin_body_background", "#D7D7BD"); ?>
			Use color: <input type = "radio" name = "css_admin_body_background_selection" value = "color"/>
		</td>
	</tr>
	<tr>
		<td>Body background-image</td>
		<td>
			<?php createImageSelector("css_admin_body_background_image"); ?>
			Use image: <input type = "radio" name = "css_admin_body_background_selection" value = "image" checked = "checked"/>
		</td>
	</tr>
	<tr>
		<td>Body font color</td>
		<td>
			<?php createColorSelection("css_admin_body_color", "#282F36"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>CONTAINER</b></td>
	</tr>
	<tr>
		<td>Container background</td>
		<td>
			<?php createColorSelection("css_admin_container_background", "#1b7e9f"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>HEADER</b></td>
	</tr>
	<tr>
		<td>Header background</td>
		<td>
			<?php createColorSelection("css_admin_header_background", "#D7D7BD"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>CONTENT</b></td>
	</tr>
	<tr>
		<td>Content background</td>
		<td>
			<?php createColorSelection("css_admin_content_background", "#f4f4f4"); ?>
		</td>
	</tr>
	<tr>
		<td>Content font color</td>
		<td>
			<?php createColorSelection("css_admin_content_color", "#282F36"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>FOOTER</b></td>
	</tr>
	<tr>
		<td>Footer background</td>
		<td>
			<?php createColorSelection("css_admin_footer_background", "#D7D7BD"); ?>
		</td>
	</tr>
	<tr>
		<td colspan = "2"><b>ERRORS</b></td>
	</tr>
	<tr>
		<td>Errors background</td>
		<td>
			<?php createColorSelection("css_admin_errors_background", "#FFEDED"); ?>
		</td>
	</tr>
	<tr>
		<td>Errors font color</td>
		<td>
			<?php createColorSelection("css_admin_errors_color", "red"); ?>
		</td>
	</tr>
	<tr>
		<td>Errors font weight</td>
		<td>
			<input type = "text" name = "css_admin_errors_weight" value = "700"/>
		</td>
	</tr>
	<tr>
		<td>
			<input type = "submit" value = "Tallenna" name = "submit"/>
			<input type = "button" value = "Takaisin" id = "back"/>
		</td>
		<?php 
			if(isset($_GET['success']))
			{
				echo "<td>";
				
				if($_GET['success'] == 1)
				{
					echo "<span style = 'color: #00FF00;'>Tallennettu!</span>";
				}
				else
				{
					echo "<span style = 'color: #FF0000;'>Virhe!</span>";
				}

				echo "</td>";
			}
			else
			{
				echo "<td></td>";
			}
		?>
	</tr>
</table>