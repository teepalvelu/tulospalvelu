/*********************************************************
* Script: Custom dynamic DOM Select-element handler
* Description: Dynamically attaches a live onchange-event
* 			   to every data:colorselector Select-element
* Author: Jonah Ahvonen
* Date: 22.4.2014
* Version: 1.0
*********************************************************/

$(document).ready(function() 
{
	//Light fonts here so we can put black font on top of them instead of white
	var specialcases = ['#D7D7BD', '#f4f4f4', '#FFEDED'];
	var selectors = 0;
	
	//Count selectors with data-selectortype of colorselector
	//and also initialize them with default background color
	$('select').each(function(i, el)
	{
		if($(el).data("selectortype") == "colorselector")
		{		
			$(el).css('background-color', $(el).data("selected"));
			selectors++;
		}
	});
	
	//Loop through the selectors and set the right background-colors and colors
	//on the <option> elements
	for(i = 0; i < selectors; i++)
	{
		$("#s" + i + " > option").each(function(item)
		{
			//Set correct option selected
			if($("#s" + i).data('selected') == $(this).val())
			{
				$(this).attr('selected', 'selected');
			}
			
			$(this).css('background-color', $(this).val());
			var v = $(this).val();
			
			if(specialcases.indexOf(v) > -1)
			{
				$(this).css('color', 'black');
			}
			else
			{
				$(this).css('color', 'white');
			}
		});
	}
	
	//Fix the font colors because of the last loop
	$('select').each(function(i, el)
	{
		if($(el).data("selectortype") == "colorselector")
		{	
			if(specialcases.indexOf($(this).val()) > -1)
			{
				$(this).css('color', 'black');
			}
			else
			{
				$(this).css('color', 'white');
			}
		}
	});
	
	//Loop through all select-elements and attach a live onchange-event
	//on them
	for(i = 0; i < selectors; i++)
	{
		$("#s" + i).on('change', function(evt)
		{
			console.log(evt.target);
			var current = $(evt.target).val();
			$(evt.target).css('background-color', current);
			
			if(specialcases.indexOf(current) > -1)
			{
				$(evt.target).css('color', 'black');
			}
			else
			{
				$(evt.target).css('color', 'white');
			}
		});  
	}
	
	$('#back').on('click', function()
	{
		window.location = "http://teepalvelu.dy.fi/tulospalvelu/";
	});
	
	// http://wowmotty.blogspot.fi/2009/06/convert-jquery-rgb-output-to-hex-color.html
	function rgbToHex(rgb)
	{
		rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		return "#" +
		("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
		("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
		("0" + parseInt(rgb[3],10).toString(16)).slice(-2);
	}

	//http://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
	function hexToRgb(hex)
	{
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	}
});