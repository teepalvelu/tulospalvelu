<?php

$data = array();

$data['db_user'] = $_POST['db_user'];
$data['db_pass'] = $_POST['db_password'];
$data['db_host'] = $_POST['db_host'];
$data['db_name'] = $_POST['db_name'];

$data['css_body_background'] = $_POST['css_body_background'];
$data['css_body_background_image'] = $_POST['css_body_background_image'];
$data['css_body_background_selection'] = $_POST['css_body_background_selection'];
$data['css_body_color'] = $_POST['css_body_color'];
$data['css_container_background'] = $_POST['css_container_background'];
$data['css_header_background'] = $_POST['css_header_background'];
$data['css_content_background'] = $_POST['css_content_background'];
$data['css_content_color'] = $_POST['css_content_color'];
$data['css_footer_background'] = $_POST['css_footer_background'];
$data['css_errors_background'] = $_POST['css_errors_background'];
$data['css_errors_color'] = $_POST['css_errors_color'];
$data['css_errors_weight'] = $_POST['css_errors_weight'];

$data['css_admin_body_background'] = $_POST['css_admin_body_background'];
$data['css_admin_body_background_image'] = $_POST['css_admin_body_background_image'];
$data['css_admin_body_background_selection'] = $_POST['css_admin_body_background_selection'];
$data['css_admin_body_color'] = $_POST['css_admin_body_color'];
$data['css_admin_container_background'] = $_POST['css_admin_container_background'];
$data['css_admin_header_background'] = $_POST['css_admin_header_background'];
$data['css_admin_content_background'] = $_POST['css_admin_content_background'];
$data['css_admin_content_color'] = $_POST['css_admin_content_color'];
$data['css_admin_footer_background'] = $_POST['css_admin_footer_background'];
$data['css_admin_errors_background'] = $_POST['css_admin_errors_background'];
$data['css_admin_errors_color'] = $_POST['css_admin_errors_color'];
$data['css_admin_errors_weight'] = $_POST['css_admin_errors_weight'];

$headerTable = array();

$dbVisited = false;
$cssVisited = false;
$bodyVisited = false;
$containerVisited = false;
$headerVisited = false;
$contentVisited = false;
$footerVisited = false;
$errorsVisited = false;
$adminVisited = false;

foreach($data as $key => $value)
{
	if($key != count($data) -1)
	{
		if(strpos($key, 'admin') !== false && !$adminVisited)
		{
			
			$bodyVisited = false;
			$containerVisited = false;
			$headerVisited = false;
			$contentVisited = false;
			$footerVisited = false;
			$errorsVisited = false;
			$adminVisited = true;
		}
		
		if(strpos($key, 'db') !== false && !$dbVisited)
		{
			echo "<b>[DATABASE]</b></br></br>";
			$dbVisited = true;
		}
		else if(strpos($key, 'css') !== false && !$cssVisited)
		{
			echo "</br><b>[CSS]</b></br>";
			$cssVisited = true;
		}
		
		if(strpos($key, 'body') !== false && !$bodyVisited)
		{
			if(!$adminVisited)
			{
				echo "</br>; BODY</br>";
			}
			else
			{
				echo "</br>; ADMIN_BODY</br>";
			}
			$bodyVisited = true;
		}
		else if(strpos($key, 'container') !== false && !$containerVisited)
		{
			if(!$adminVisited)
			{
				echo "</br>; CONTAINER</br>";
			}
			else
			{
				echo "</br>; ADMIN_CONTAINER</br>";
			}
			$containerVisited = true;
		}
		else if(strpos($key, 'header') !== false && !$headerVisited)
		{
			if(!$adminVisited)
			{
				echo "</br>; HEADER</br>";
			}
			else
			{
				echo "</br>; ADMIN_HEADER</br>";
			}
			$headerVisited = true;
		}
		else if(strpos($key, 'content') !== false && !$contentVisited)
		{
			if(!$adminVisited)
			{
				echo "</br>; CONTENT</br>";
			}
			else
			{
				echo "</br>; ADMIN_CONTENT</br>";
			}
			$contentVisited = true;
		}
		else if(strpos($key, 'footer') !== false && !$footerVisited)
		{
			if(!$adminVisited)
			{
				echo "</br>; FOOTER</br>";
			}
			else
			{
				echo "</br>; ADMIN_FOOTER</br>";
			}
			$cfooterVisited = true;
		}
		else if(strpos($key, 'errors') !== false && !$errorsVisited)
		{
			if(!$adminVisited)
			{
				echo "</br>; ERRORS</br>";
			}
			else
			{
				echo "</br>; ADMIN_ERRORS</br>";
			}
			$errorsVisited = true;
		}
		
		echo $key . "\t->\t" . $value . "</br>";
	}
}

$dbVisited = false;
$cssVisited = false;
$bodyVisited = false;
$containerVisited = false;
$headerVisited = false;
$contentVisited = false;
$footerVisited = false;
$errorsVisited = false;
$adminVisited = false;

$file = fopen('../includes/settings.ini', 'w');
fwrite($file, '; Tulospalvelu settings' . n2());

foreach($data as $key => $value)
{
	if($key != count($data) -1)
	{
		if(strpos($key, 'admin') !== false && !$adminVisited)
		{
			
			$bodyVisited = false;
			$containerVisited = false;
			$headerVisited = false;
			$contentVisited = false;
			$footerVisited = false;
			$errorsVisited = false;
			$adminVisited = true;
			
			fwrite($file, n() . "[CSS_ADMIN]" . n());
		}
		
		if(strpos($key, 'db') !== false && !$dbVisited)
		{
			fwrite($file, "[DATABASE]" . n2());
			$dbVisited = true;
		}
		else if(strpos($key, 'css') !== false && !$cssVisited)
		{
			fwrite($file, n() . "[CSS]" . n());
			$cssVisited = true;
		}
		
		if(strpos($key, 'body') !== false && !$bodyVisited)
		{
			if(!$adminVisited)
			{
				fwrite($file, n() . "; BODY" . n());
			}
			else
			{
				fwrite($file, n() . "; ADMIN_BODY" . n());
			}
			$bodyVisited = true;
		}
		else if(strpos($key, 'container') !== false && !$containerVisited)
		{
			if(!$adminVisited)
			{
				fwrite($file, n() . "; CONTAINER" . n());
			}
			else
			{
				fwrite($file, n() . "; ADMIN_CONTAINER" . n());
			}
			$containerVisited = true;
		}
		else if(strpos($key, 'header') !== false && !$headerVisited)
		{
			if(!$adminVisited)
			{
				fwrite($file, n() . "; HEADER" . n());
			}
			else
			{
				fwrite($file, n() . "; ADMIN_HEADER" . n());
			}
			$headerVisited = true;
		}
		else if(strpos($key, 'content') !== false && !$contentVisited)
		{
			if(!$adminVisited)
			{
				fwrite($file, n() . "; CONTENT" . n());
			}
			else
			{
				fwrite($file, n() . "; ADMIN_CONTENT" . n());
			}
			$contentVisited = true;
		}
		else if(strpos($key, 'footer') !== false && !$footerVisited)
		{
			if(!$adminVisited)
			{
				fwrite($file, n() . "; FOOTER" . n());
			}
			else
			{
				fwrite($file, n() . "; ADMIN_FOOTER" . n());
			}
			$cfooterVisited = true;
		}
		else if(strpos($key, 'errors') !== false && !$errorsVisited)
		{
			if(!$adminVisited)
			{
				fwrite($file, n() . "; ERRORS" . n());
			}
			else
			{
				fwrite($file, n() . "; ADMIN_ERRORS" . n());
			}
			$errorsVisited = true;
		}
		
		fwrite($file, $key . " = " . $value . n());
	}
}

fclose($file);
header('Location: index.php?success=1');

function n()
{
	return PHP_EOL;
}

function n2()
{
	return PHP_EOL . PHP_EOL;
}











?>