﻿<!DOCTYPE html>
<html>
	<head>
		<title>Tulospalvelu - Installer</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="css/installerstyle.css">
		<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="script/installerscript.js"></script>
	</head>
	<body>
		<div id = "wrapper">
		
			<h1>Tulospalvelu - Installer</h1>
			<hr>
			
			<div id = "content">
				<form action = "iniwriter.php" method = "POST" name = "installerform">
					<?php require_once('installer_table.php'); ?>
				</form>
			</div>
		</div>
	</body>
</html>

<?php

function createColorSelection($name, $default)
{
	static $id = 0;
	
	echo "<select name = '$name' id = 's$id' data-selectortype = 'colorselector' data-selected = '$default'>\n";
	echo "<option value = '#D7D7BD'>Tana</option>\n";
	echo "<option value = '#34495e'>Wet asphalt</option>\n";
	echo "<option value = '#2c3e50'>Midnight blue</option>\n";
	echo "<option value = '#95a5a6'>Concrete</option>\n";
	echo "<option value = '#7f8c8d'>Asbestos</option>\n";
	echo "<option value = '#282F36'>Outer space</option>\n";
	echo "<option value = '#1b7e9f'>Matisse</option>\n";
	echo "<option value = '#f4f4f4'>Wild sand</option>\n";
	echo "<option value = '#FFEDED'>Fair pink</option>\n";
	echo "<option value = 'red'>Red</option>\n";
	echo "</select>\n";
	
	$id++;
}

function createImageSelector($name)
{
	$dir = "backgrounds/";

	if (is_dir($dir)) 
	{
		if ($dh = opendir($dir)) 
		{
			$images = array();

			while (($file = readdir($dh)) !== false) 
			{
				if (!is_dir($dir.$file)) 
				{
					$images[] = $file;
				}
			}
			closedir($dh);
		}
	}
	
	echo "<select name = '$name' class = 'imageselector'>";
	
	foreach($images as $key => $value)
	{
		echo "<option value = '$value'>$value</option>";
	}
	
	echo "</select>";
}

?>