<!DOCTYPE html>
<html>
	<head>
		<title>Tulospalvelu - webdoc</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="webdocstyle.css">
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="3dcontrols.js"></script>
	</head>
	<body>
		<h1><a id="top">Tulospalvelu - webdoc</a></h1>
		
		<h2><a id="1">1. Sisältö</a></h2>
			<ol class = "padded">
				<li><a href="#1" class = "contentlink">Sisältö</a></li>
				<li><a href="#2" class = "contentlink">Tekijät</a></li>
				<li><a href="#3" class = "contentlink">Resurssit</a></li>
				<li><a href="#4" class = "contentlink">Yleisesti</a></li>
				<li><a href="#5" class = "contentlink">Kuvia</a></li>
				<li><a href="#6" class = "contentlink">Tietokanta</a></li>
				<li><a href="#7" class = "contentlink">Rakenne / yhteydet / arkkitehtuuri</a></li>
				<li><a href="#8" class = "contentlink">Tekniset asiat</a></li>
				<li><a href="#9" class = "contentlink">Ilmenneet ongelmat & niiden ratkaisut</a></li>
				<li><a href="#10" class = "contentlink">Arvosanaehdotukset & työtunnit</a></li>
			</ol>
			
		<h2><a id="2">2. Tekijät</a></h2>
			<table class = "paddedtable">
				<tr>
					<td>
						Teemu Kontio
					</td>

					<td>
					G2710
					</td>
				</tr>
				<tr>
					<td>
						Jonah Ahvonen
					</td>
					<td>
					G7902
					</td>
				</tr>
				<tr>
					<td>
						<span id = "matti">Matti Jokitulppo</span>
					</td>
					<td>
					G8049
					</td>
				</tr>
			</table>
			
		<h2><a id="3">3. Resurssit</a></h2>
			<table class = "paddedtable">
				<tr>
					<td>
						Tietokanta:
					</td>
					<td>
						MySQL
					</td>
				</tr>
				<tr>
					<td>
						Kehityskielet:
					</td>
					<td>
						PHP, JavaScript, HTML5 & CSS
					</td>
				</tr>
				<tr>
					<td>
						Kirjastot ja frameworkit:
					</td>
					<td>
						JQuery & Bootstrap
					</td>
				</tr>
				<tr>
					<td>
						Muut:
					</td>
					<td>
						Notepad++ & Sourcetree
					</td>
				</tr>
			</table>
				
		<h2><a id="4">4. Yleisesti</a></h2>
			<p class = "content">
				Tulospalvelu tehtiin jokaisen osalta WWW-palvelinohjelmointi (IIM50300) -kurssia varten (Matti, Teemu, Jonah),<br>
				mutta se toimii myös WWW-tekniikat (IIO10410) -kurssin harjoitustyönä joidenkin ryhmäläisten osalta (Matti, Teemu).<br><br>
				
				Tulospalvelua on kehitetty mihin tahansa sijoitettavana sovelluksena, joten geneerisyys ja muokattavuus on pyritty<br>
				ottamaan huomioon. Tämä näkyy selkeästi jo "Tulospalvelu" -nimessä sillä ei ole määritelty minkä lajin tulospalvelu on kyseessä. Lisäksi<br>
				muokattavuuden kannalta tärkeitä muuttujia on eristetty sovelluksen logiikasta erilleen, jotta niiden säätäminen olisi helpompaa.<br><br>
				
				"Tulospalvelu"-nimestä voidaan myös päätellä, että kyseessä on jonkinlainen tuloksia käsittelevä palvelu. Sanalla "palvelu" tarkoitetaan yleensä <br>
				jonkinlaista aineetontä hyödykettä, tai korvausta vastaan suoritettua tekoa joka tehdään toisen puolesta.
				
			</p>
			
		<h2><a id="5">5. Kuvia</a></h2>
			<h3>3.1 Tulosten hakeminen</h3>
				<p class = "content">
					<img src = "res/tulosten_haku.png" alt = "tulosten haku.png" class = "roundedimage" style = "width: 800px;"/>
				</p>
				
			<h3>3.2 Admin paneeli</h3>
				<p class = "content">
					<b>Admin paneelin login:</b><br>
					<img src = "res/admin_panel_login.png" alt = "admin_login" class = "roundedimage" style = "width: 800px;"/><br><br>
					
					<b>Admin paneelin päänäkymä:</b><br>
					<img src = "res/admin_panel_main.png" alt = "admin_main" class = "roundedimage" style = "width: 800px;"/><br><br>
					
					<b>Turnauksen lisääminen:</b><br>
					<img src = "res/admin_panel_turnaus.png" alt = "admin_turnaus" class = "roundedimage" style = "width: 800px;"/><br><br>
					
					<b>Käyttäjien hallintapaneeli:</b><br>
					<img src = "res/admin_panel_kayttajat.png" alt = "admin_kayt" class = "roundedimage" style = "width: 800px;"/><br><br>
				</p>
		
		<h2><a id="6">6. Tietokanta</a></h2>
			<p class = "content">
				<a href = "tk.php" class = "contentlink">Tietokannan webdoc</a>
			<p>
			
		<h2><a id="7">7. Rakenne / yhteydet / arkkitehtuuri</a></h2>
			<p class = "content">
				<img src = "res/hieno_yhteys_kaavio.png" alt = "yht_kaavio" class = "roundedimage" style = "width: 800px;"/>
			<p>	
			
		<h2><a id="8">8. Tekniset asiat</a></h2>
				<h3>8.1 ShowContent-funktio</h3>
				<p class = "content">
				Kaikki sivun sisältö taiotaan esiin sekä admin- että käyttäjäpuolella functions.php:n ShowContent()-funktiolla. Aluksi parsitaan<br>
				URL:sta haluttu sivu, jonka niminen .php tiedosto includetaan sisällöksi. Admin-puolen ShowContent() lisäksi ottaa erikseen <br>
				vasemmalla näkyvän menu-palkin, sekä oikealla näkyvän view-sisällön.
				<p>
				
				<h3>8.2 DatabaseController & AdminDatabaseController-luokat</h3>
				<p class = "content">
				Tulospalvelu on loppujen lopuksi aika CRUD-painotteinen sovellus, joten DatabaseController-oliot ovat luonnollisesti suuri osa<br>
				sitä. Luokkien konstruktorissa otetaan yhteyttä tietokantaan, ja luokilla on monta erilaista metodia, jotka hakevat kyselyillä<br>
				tietoa tietokannasta.
				<p>	
				<h3>8.3 Error-luokka</h3>
				<p class = "content">
				Error-luokan tarkoituksena oli alunperin toimia virheviestien näyttämiseen käyttäjälle miellyttävällä tavalla. Nyt virheviestien<br>
				lisäksi se myös näyttää onnistuneista toiminnoista ilmoituksen. Luokka pitää vain huolta viesteistä, jotka parsitaan ja <br>
				näytetään käyttäjälle parseErrors-funktiossa.
				<p>
				<h3>8.4 "Installer"</h3>
				<p class = "content">
				Hieman hämäävästi nimetty Installer ei varsinaisesti asenna mitään, vaan sen avulla pystyy muokkaamaan sivuston asetuksia. Käytännössä<br>
				tämä tarkoittaa tietokannan yhteystietojen muuttamista, sekä sivuston ulkoasun muokkaamista.
				<p>	
				<h3>8.5 Breadcrumbs</h3>
				<p class = "content">
				Kun käyttäjä seikkailee sivustolla turnauksesta sarjoihin ja sarjoista lohkoihin, näytetään hänelle hänen sijaintinsa ruudun ylälaidassa.<br>
				Tämä tehdään resolver-funktion avulla. Funktio ottaa parametrikseen sivuston tyypin (esim. "tournament") sekä ID:n jolla kyseinen turnaus/sarja/<br>
				mikä tahansa löytyy tietokannasta. Sitten switch-case rakenteessa katsotaan, minkä tyyppistä sivustoa tarkastellaan, ja tulostetaan oikea polku<br>
				käyttäjälle.
				<p>		
				<h3>8.6 Webdoc</h3>
				<p class = "content">
				Käyttämämme webbidokumentti, jota luet paraikaa, tulee valehtelematta mullistamaan dokumentoinnin niinkuin me sen tiedämme. jQueryn ansiosta<br>
				webbidokumentaatiomme pystyy hallitsemaan ja liikuttamaan kaikissa ihmisen havaitsemissa ulottuvuuksissa, ajoittain jopa useammissa.<br>
				Hiiren oikealla pystyt liikuttelemaan näkymää, CTRL pohjassa ja rullaamalla pystyt zoomaamaan, Q ja E pyörittävät dokumentaatiota sivuttain<br>
				ja napauttamalla hiiren rullaa pystyt peilailemaan dokumentaatio kaikissa ulottuvuuksissa.
				<p>
				<h3>8.7 LiveGames</h3>
				<p class = "content">
				JavaScriptin ja AJAX:in avulla haemme sivuston etusivulla otteluiden live-statusta. Joka 5 sekunti live_games.js kutsuu AJAX:lla<br>
				fetch_live_games.php:ltä tuloksia, jotka se tunkee etusivun livegames-diviin.
				<p>					
		<h2><a id="9">9. Ilmenneet ongelmat & niiden ratkaisut</a></h2>
			<h3>Tietokanta</h3>
				<p class = "content">
					Tietokantaa suunniteltiin pitkään, mutta alkusuunnittelunkin jälkeen täytyi tulospalvelua kehittäessä<br>
					tehdä joitain pieniä muutoksia tietokantaan ja sen rakenteeseen. Lisäksi nykyinen tietokantarakenne johtaa<br>
					pitkiin kyselyihin, mutta ne kun kerran tekee niin sen jälkeen se on melkeimpä ihan sama.
				</p>		
			
			<h3>Sivustoarkkitehtuuri</h3>
				<p class = "content">
					Sivuston arkkitehtuuri / rakenne oli jonkun aikaan pimennossa. Päädyimme lopulta "menu - view" -tyyppiseen ratkaisuun,<br>
					eli sivuston vasempaan laitaan rakennetaan menu ja oikealle sisällöksi rakennetaan tietokantakyselyjen avulla käyttäjän pyytämä<br>
					view.
				</p>	
			
			<h3>URL</h3>
				<p class = "content">
					Haluttiin tehdä URL:sta "kauniimpi" tyylillä "www.example.com/a/b/c" sen sijaan että olisi tehty "www.example.com?a=1&ampb=1&ampc=1".<br>
					Tämä johti paikoitellen sivuston outoon toimintaan, kuten esimerkiksi duplikaatti-includeihin, mutta ongelmat saatiin ratkaistua<br>
					alkuperäistä ideaa hieman muuttamalla.
				</p>	
				
			<h3>Versionhallinta</h3>
				<p class = "content">
					Versionhallinta tulospalvelua kehittäessä ei ollut ideaalista. Pahimmillaan se johti suuriin määriin "fix", "fix2", "fix3" -tyylisiin<br>
					committeihin, sillä palvelua oli hankala kehittää paikallisesti ja ainakin osa testauksesta täytyi suorittaa työntämällä uusin versio<br>
					sivustosta versionhallintaan, josta se automaattisesti kopioituu palvelimelle.
				</p>
			
			<h3>CSS</h3>
				<p class = "content">
					<img src = "http://2.bp.blogspot.com/-41v6n3Vaf5s/UeRN_XJ0keI/AAAAAAAAN2Y/YxIHhddGiaw/s1600/css.gif" alt = "hauska gif"/>
				</p>
		
		<h2><a id="10">10. Arvosanaehdotukset & työtunnit</a></h2>
		<p class = "content">
			<h3 style = "color: #e74c3c;">Teemu Kontio</h3>
			<p class = "content">
				<b>WWW-palvelinohjelmointi (IIM50300):</b><br>
				5, mielestäni tekemämme kurssityö täyttää ja ylittää kurssityölle asetetut tavoitteet.<br>
				Tekemämme kurssityö on erittäin laaja käyttökelpoinen sovellus. Lisäksi opin paljon uutta työstäessämme projektia.<br>
				<br><br><b>WWW-tekniikat (IIO10410):</b><br>
				5, perus WWW-tekniikat tulevat selkeästi esille kurssityössä. Työssä käytetty mm. BootStrappiä ja muutenkin hyödynnetty Javascriptiä<br>
				soveltuvin osin. Tuotettu käyttäjäpuolen sivusto menee kauniisti läpi HTML5 validoinnista.
			</p>
			
			<h3 style = "color: #e74c3c;">Jonah Ahvonen</h3>
			<p class = "content">
				<b>WWW-palvelinohjelmointi (IIM50300):</b><br>
				5, koska osasin kurssin asiat pääpiirteittäin jo entuudestaan ja tästä tulospalvelusta tuli aika huikea.<br>
				Suurin osa omasta ajastani kului erilaisten lisäominaisuuksien luomisessa ja JavaScriptin kanssa koodaillessa.
				<br><br><b>WWW-tekniikat (IIO10410):</b><br>
				-
			</p>
				
			<h3 style = "color: #e74c3c;">Matti Jokitulppo</h3>
			<p class = "content">
				<b>WWW-palvelinohjelmointi (IIM50300):</b><br>
				5, koska tunnen oppineeni PHP:n salat kurssin aikana tosi hyvin, ja uskon että harjoitustyömme on tarpeeksi laaja sekä laadukas todistaakseen tämän. <br>
				Harjoitustyötä tehdessä opin Teemulta ja Jonahilta hyödyllisiä kikkoja, joita ei tavallisessa opetuksessa käyty, esimerkkinä vaikkapa $commandien pilkkominen<br>
				URL:sta
				<br><br><b>WWW-tekniikat (IIO10410):</b><br>
				5, koska mielestäni saimme aikaan ihan hyvännäköisen ja toimivan sivuston. Vaikka työ on aika PHP-painotteinen, saimme JavaScriptiäkin mukaan kokonaisuuteen live-pelien<br>
				hakemisen sekä tämän webdocin hienojen pyörittelykontrollien myötä.
			</p>	
			
			<h3 style = "color: #e74c3c;">Työtunnit</h3>
			
			<table class = "borderedpaddedtable">
				<tr>
					<th>Tekijä</th>
					<th>DatabaseController</th>
					<th>AdminDatabaseController</th>
					<th>Käyttäjäpuoli</th>
					<th>Admin-viewit</th>
					<th>Admin-menut</th>
					<th>CSS</th>
					<th>JavaScript</th>
					<th>Dokumentointi</th>
					<th>Suunnittelu</th>
					<th>Yhteensä</th>
				</tr>
				<tr>
				<td>Teemu Kontio</td>
				<td>2 h</td>
				<td>12 h</td>
				<td>2 h</td>
				<td>9 h</td>
				<td>5 h</td>
				<td>1 h</td>
				<td>1 h</td>
				<td>2 h</td>
				<td>7 h</td>
				<td>41 h</tr>
				</tr>
				<tr>
				<td>Jonah Ahvonen</td>
				<td>1 h</td>
				<td>0 h</td>
				<td>2 h</td>
				<td>0 h</td>
				<td>6 h</td>
				<td>4 h</td>
				<td>14 h</td>
				<td>6 h</td>
				<td>6 h</td>
				<td>39 h</tr>
				</tr>
				<tr>
				<td>Matti Jokitulppo</td>
				<td>3 h</td>
				<td>9 h</td>
				<td>2 h</td>
				<td>9 h</td>
				<td>9 h</td>
				<td>2 h</td>
				<td>0 h</td>
				<td>4 h</td>
				<td>3 h</td>
				<td>41 h</tr>
				</tr>
			</table>
		</p>
		<hr>
		<a href="#top" class = "backtotop">Back to top</a>
	</body>
</html>