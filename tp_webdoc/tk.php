<!DOCTYPE html>
<html>
	<head>
		<title>Tulospalvelutietokanta - webdoc IIZO3030 Tietokannat</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="webdocstyle.css">
		<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src="3dcontrols.js"></script>
	</head>
	<body>
		<h1><a id="top">Tulospalvelutietokanta - webdoc - IIZO3030 Tietokannat</a></h1>
		
		<h2><a id="1">1. Sisältö</a></h2>
			<ol class = "padded">
				<li><a href="#1" class = "contentlink">Sisältö</a></li>
				<li><a href="#2" class = "contentlink">Tekijät</a></li>
				<li><a href="#3" class = "contentlink">Yleisesti</a></li>
				<li><a href="#4" class = "contentlink">Ilmenneet ongelmat & niiden ratkaisut</a></li>
				<li><a href="#5" class = "contentlink">Resurssit</a></li>
				<li><a href="#6" class = "contentlink">Arvosanaehdotukset</a></li>
				<li><a href="#7" class = "contentlink">Tietokantarakenne</a></li>
			</ol>
			
		<h2><a id="2">2. Tekijät</a></h2>
			<table class = "paddedtable">
				<tr>
					<td>
						Teemu Kontio
					</td>

					<td>
					G2710
					</td>
				</tr>
				<tr>
					<td>
						Jonah Ahvonen
					</td>
					<td>
					G7902
					</td>
				</tr>
				<tr>
					<td>
						<span id = "matti">Matti Jokitulppo</span>
					</td>
					<td>
					G8049
					</td>
				</tr>
			</table>
			
		<h2><a id="3">3. Yleisesti</a></h2>
			<p class = "content">
				Tulospalvelutietokanta on tehty JAMKin tietokanta-, palvelinohjelmointi- ja www-tekniikat -kursseja varten.<br>
				
				Tietokantaa käyttää ensisijaisesti tekemämme tulospalvelu-sivusto, jota käytetään erilaisten kilpailutulosten esittämiseen.</br>
				Pyrimme tietokantarakenteessa mm. geneerisyyteen, jotta sitä voisi käyttää minkä tahansa kilpailutulosjoukon tallentamiseen.
	
				<br><br>
			</p>
		
				<h2><a id="4">4. Ilmenneet ongelmat & niiden ratkaisut</a></h2>
			<p class = "content">
				Ainut merkittävä työnteossa ilmenneet hankaluudet koskivat alkuperäistä suunnittelutyötä. Turnaus -> Lohko<br>
				-> Sarja käsitteiden toteuttaminen ilman, että tietokantaan tarvitsisi lisätä "turhaa" dataa, aiheutti aluksi<br>
				hieman päänvaivaa, mutta tunnemme, että lopullinen ratkaisumme vastaa näihin ongelmiin elegantisti.<br><br>
			</p>
		
		<h2><a id="5">5. Resurssit</a></h2>
			<table class = "paddedtable">
				<tr>
					<td>
						Tietokanta:
					</td>
					<td>
						MySQL 5.5.35
					</td>
				</tr>
				<tr>
					<td>
						Tietokantamoottori:
					</td>
					<td>
						innoDB 5.5.35
					</td>
				</tr>
				<tr>
					<td>
						Apuohjelma(t):
					</td>
					<td>
						MySQL Workbench 5.2 CE
					</td>
				</tr>
			</table>
		
		<h2><a id="6">6. Arvosanaehdotukset</a></h2>
		<p class = "content">
			<h2>Teemu Kontio</h2>
			<p class = "content">Ehdotan itselleni arvosanaksi 5, koska tunnen hallitsevani kurssin asiat todella hyvin, ellei jopa erinomaisesti.</p>
			<h2>Jonah Ahvonen</h2>
			<p class = "content">Ehdotan itselleni arvosanaksi 5, koska tunnen hallitsevani kurssin asiat todella hyvin, ellei jopa erinomaisesti.</p>
			<h2>Matti Jokitulppo</h2>
			<p class = "content">Ehdotan itselleni arvosanaksi 5, koska tunnen hallitsevani kurssin asiat todella hyvin, ellei jopa erinomaisesti.</p>
		</p>	
		
		<h2><a id="7">7. Tietokantarakenne</a></h2>
		<p class = "content">
			<img src = "http://www.imgur.com/ThUsxTL.png" alt = "Rakennekuva on hävinnyt :(" class = "roundedimage"/>
		</p>		
		<hr>
		<a href="#top" class = "backtotop">Back to top</a>
	</body>
</html>