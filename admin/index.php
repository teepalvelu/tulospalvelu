﻿<?php
error_reporting(-1);
if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header("Location: $redirect");
}


	require_once('includes/startup.php');
	require_once('includes/functions.php');
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Tulospalvelu - Admin</title>
		<meta charset="UTF-8">
		<link href="https://fonts.googleapis.com/css?family=Exo+2:400,300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="<?php echo getBasePath();?>/../scripts/dist/css/bootstrap.css">
		<!--<link rel="stylesheet" type="text/css" href="<?php echo getBasePath();?>/../css/styles.css">-->
		<?php include('../css/admincss.php'); ?>
	</head>
	<body>
		<div id="container">	
			<div id="header">
				<h1 class = "header"><?php echo '<a href="'.getBasePath().'/tournamentlist/">'; ?>Tulospalvelu Admin</a></h1>
			</div>
			
			<div id="content">
			<ol class="breadcrumb">
					<?php
					echo '<li><a href="'.getBasePath().'/tournamentlist/">Admin</a></li>';
					$c = getCommands();
					@$crumbarray = resolver($c[0],$c[1]);
					if (is_array($crumbarray)){
						foreach($crumbarray as $key => $step){
								echo '<li><a href="'.getBasePath().'/'.$key.'/'.$step["id"].'">'.$step["name"].'</a></li>';
						}
					}
					?>
				</ol>
				<p><?php showContent();?></p>
			</div>
			
			<div id="footer">
				<!--<h1 class = "footer">Le' alapalkki</h1>-->
				<p class = "left">Le' alapalkki</p>
				<p class = "right"><a href = "../">Takaisin</a></p>
			</div>	
		</div>
		<script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script src = "<?php echo getBasePath();?>/../scripts/dist/js/bootstrap.js"></script>
	</body>
</html>