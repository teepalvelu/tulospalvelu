<?php
if(isset($_POST['editmatch'])){
	if($_POST['day'] != "" && $_POST['month'] != "" && $_POST['year'] != "" && $_POST['hour'] != "" && $_POST['minute'] != "" && $_POST['homepoints'] != "" && $_POST['visitorpoints'] != "" && $_POST['gamestate'] != "" && $_POST['field'] != "" && $_POST['hometeam'] != ""  && $_POST['visitorteam'] != ""){
		if($_POST["hometeam"] != $_POST["visitorteam"]){
			for($i=1;$i<=sizeof($_POST["homepoints"]);$i++){
				$_POST["homepoints"][$i] == "" ? $homescore = null : $homescore = $_POST["homepoints"][$i];
				$_POST["visitorpoints"][$i] == "" ? $visitorscore = null : $visitorscore = $_POST["visitorpoints"][$i];
				$GLOBALS['database']->updateScore($_POST["gid"],$homescore,$visitorscore,$i);
			}
			$GLOBALS['database']->updateMatch($_POST["gid"], $_POST["gamestate"], $_POST["hometeam"] , $_POST["visitorteam"], $_POST["field"], date ("Y-m-d H:i:s", mktime($_POST["hour"],$_POST["minute"],0,$_POST["month"],$_POST["day"],$_POST["year"])));
		}else{
			$GLOBALS['error']->setError("Joukkue ei voi pelata itseään vastaan.");
		}
	}else{
		$GLOBALS['error']->setError("Jokin tieto puuttui tai oli tyhjä");
	}
}

$info = $GLOBALS['database']->getSingleGameById($command[1])->fetch();
$divisioninfo = $GLOBALS['database']->getDivisionById($info["division_id"])->fetch();
$settings = $GLOBALS['database']->getSettings($divisioninfo["division_setting_id"])->fetch();
echo '<div style="text-align: center;">';
		echo '<form action="" method="post">';
		echo '<h2>Muokkaa ottelua</h2>';
			echo '<table style="margin: 2px auto;">';
				echo '<tr><td>Aika:</td><td>';
					echo '<select name="day">';
						for($i = 1; $i<=31;$i++){
							echo '<option value="'.$i.'"';
								if(date('j',strtotime($info["game_time"])) == $i){
									echo 'selected';
								}
							echo '>'.$i.'</option>';
						}
					echo '</select>';
					echo '<select name="month">';
						for($i = 1; $i<=12;$i++){
							echo '<option value="'.$i.'"';
								if(date('n',strtotime($info["game_time"])) == $i){
									echo 'selected';
								}
							echo '>'.$i.'</option>';
						}
					echo '</select>';
					echo '<select name="year">';
						for($i = date('Y')-1; $i<=date('Y')+1;$i++){
							echo '<option value="'.$i.'"';
								if(date('Y',strtotime($info["game_time"])) == $i){
									echo 'selected';
								}
							echo '>'.$i.'</option>';
						}
					echo '</select> ';
					echo '<select name="hour">';
						for($i = 0; $i<=23;$i++){
							echo '<option value="'.$i.'"';
								if(date('G',strtotime($info["game_time"])) == $i){
									echo 'selected';
								}
							echo '>'.$i.'</option>';
						}
					echo '</select>';
					echo '<select name="minute">';
						for($i = 0; $i<=59;$i++){
						if($i < 10){$iform = "0"; $iform .=$i;}else{$iform = $i;}
							echo '<option value="'.$i.'"';
								if(date('i',strtotime($info["game_time"])) == $iform){
									echo 'selected';
								}
							echo '>'.$iform.'</option>';
						}
					echo '</select>';
				echo '</td></tr>';
				$teamDiv = $GLOBALS['database']->teamDivResolver($info['team1_id'])->fetch();
				echo '<tr><td>Kotijoukkue: </td><td><select name="hometeam">';
						 foreach($GLOBALS['database']->getTeams($teamDiv['division_id']) as $row)
							 {
								 if ($row['team_id'] == $info['team1_id'])
								 {
									echo '<option value="'.$row['team_id'].'" selected>' . $row['team_name'] . '</option>';
								 }
								 else
								 {
									echo '<option value="'.$row['team_id'].'">' . $row['team_name'] . '</option>';
								 }
							 }	
				echo '</select></tr>';
				echo '<tr><td>Vierasjoukkue: </td><td><select name="visitorteam">';
						 foreach($GLOBALS['database']->getTeams($teamDiv['division_id']) as $row)
							 {
								 if ($row['team_id'] == $info['team2_id'])
								 {
									echo '<option value="'.$row['team_id'].'" selected>' . $row['team_name'] . '</option>';
								 }
								 else
								 {
									echo '<option value="'.$row['team_id'].'">' . $row['team_name'] . '</option>';
								 }
							 }	
				echo '</select></tr>';
				echo '<tr><td>Kenttä: </td><td><select name="field">';
						 foreach($GLOBALS['database']->getFields() as $row)
							 {
								 if ($row['field_id'] == $info['field_id'])
								 {
									echo '<option value="'.$row['field_id'].'" selected>' . $row['field_name'] . '</option>';
								 }
								 else
								 {
									echo '<option value="'.$row['field_id'].'">' . $row['field_name'] . '</option>';
								 }
							 }	
				echo '</select></tr>';
				echo '<tr><td>Tulos:</td><td>';
				for($i=1;$i<=$settings["setting_gameType"];$i++){
				$score = $GLOBALS['database']->getScore($command[1], $i)->fetch();
					echo '<input type="text" name="homepoints['.$i.']" size="3" value="'.$score["score_team1"].'"> - ';
					echo '<input type="text" name="visitorpoints['.$i.']" size="3" value="'.$score["score_team2"].'"><br>';
				}
				echo '</td></tr>';
				echo '<tr><td>Pelitila: </td><td><select name="gamestate">';
				echo '<option value="0"';
				if($info["game_state"] == 0){echo 'selected';}
				echo'>Pelaamatta</option>';
				echo '<option value="1"';
				if($info["game_state"] == 1){echo 'selected';}
				echo '>Pelattu</option>';
				echo '</select></td></tr>';
				echo '<input type="hidden" name="gid" value="'.$command[1].'">';
				echo '<tr><td></td><td><input type="submit" class="btn-info" name="editmatch" value="Tallenna"></td></tr>';
			echo '</table>';
		echo '</form>';
		echo '</div>';
?>

