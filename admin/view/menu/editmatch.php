<?php
$backbutton = $GLOBALS['database']->gameDivResolver($command[1])->fetch();

echo '<ul class="list-group">';
		echo '<a href="'.getBasePath().'/division/'.$backbutton['division_id'].'"><li class="list-group-item">Takaisin lohkoon</li></a>';
echo '</ul>';
echo'
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">Muokkaa ottelua</h3>
  </div>
  <div class="panel-body">
    - Voit muuttaa ottelun aikaa, joukkueita sekä kenttää!<br>
	- Tulos-laatikoihin laitetaan erätulokset<br>
	- Pelitila kertoo onko ottelu pelattu vai ei.
  </div>
</div>
';
?>