<?php
if(isset($_POST['editdivision'])){
	if($_POST['name'] != "" && $_POST['winpt'] != ""  && $_POST['tiept'] != "" &&  $_POST['losept'] != "" && $_POST['gametype'] != ""){
		if (ctype_digit($_POST['winpt']) && ctype_digit($_POST['tiept']) && ctype_digit($_POST['losept']) && ctype_digit($_POST['gametype'])){
			$GLOBALS['database']->editDivision($_POST['name'], $_POST['did']);
			$divisioninfo = $GLOBALS['database']->getDivisionById($info["division_id"])->fetch();
			$GLOBALS['database']->editSettings($_POST['winpt'],$_POST['tiept'],$_POST['losept'],$_POST['gametype'],$divisioninfo["division_setting_id"]);
		}
		else{
		$GLOBALS['error']->setError("Kerrointen tulee olla numeroita!");
		}
	}else{
		$GLOBALS['error']->setError("Jokin tieto puuttui tai oli tyhjä");
	}
}
$divisioninfo = $GLOBALS['database']->getDivisionById($command[1])->fetch();
$settings = $GLOBALS['database']->getSettings($divisioninfo["division_setting_id"])->fetch();
echo '<div style="text-align: center;">';
		echo '<form action="" method="post">';
		echo '<h2>Muokkaa lohkoa</h2>';
			echo '<table style="margin: 2px auto;">';
				echo '<tr><td>Nimi:</td><td><input type="text" name="name" value="'.$divisioninfo["division_name"].'"></td></tr>';
				echo '<tr><td>Pelityyppi:</td><td>';
				echo '<select name="gametype">';
					echo '<option value = "1" ';
						if($settings["setting_gameType"] == 1){echo 'selected';}
					echo '>Yksi jakso (aikapeli)</option>';
					echo '<option value = "2" ';
						if($settings["setting_gameType"] == 2){echo 'selected';}
					echo '>Kaksi jaksoa</option>';
					echo '<option value = "3" ';
						if($settings["setting_gameType"] == 3){echo 'selected';}
					echo '>Kaksi jaksoa + (s/k)</option>';
					echo '<option value = "4" ';
						if($settings["setting_gameType"] == 4){echo 'selected';}
					echo '>Kaksi jaksoa + s + k</option>';
				echo '</select>';
				echo'</td></tr>';
				echo '<tr><td colspan="2">Pisteasetukset</td></tr>';
				echo '<tr><td>Voitto:</td><td><input type="number" name="winpt" value="'.$settings["setting_gameWinPt"].'"></td></tr>';
				echo '<tr><td>Tasapeli:</td><td><input type="number" name="tiept" value="'.$settings["setting_gameTiePt"].'"></td></tr>';
				echo '<tr><td>Häviö:</td><td><input type="number" name="losept" value="'.$settings["setting_gameLosePt"].'"></td></tr>';
				echo '<tr><td></td><td><input type="submit" class="btn-info" name="editdivision" value="Tallenna"></td></tr>';
			echo '</table>';
			echo '<input type="hidden" name="did" value="'.$command[1].'">';
		echo '</form>';
		echo '</div>';
?>