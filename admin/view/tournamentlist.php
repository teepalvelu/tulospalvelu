<?php
	echo '<h3>Turnaukset</h3>';
	echo '<table>';
		foreach($GLOBALS['database']->getTournaments() as $row) {
					echo '<tr><td width="440px"><a href="'.getBasePath().'/tournament/'.$row["t_id"].'">';
						echo '<button style="width:440px; text-align: left;" type="button" class="btn btn-default btn-sm">['.date('d.m.Y',strtotime($row["t_time"])).'] '.$row["t_name"].'</button>';
					echo '</a></td>';
					echo '<td><a href="'.getBasePath().'/edittournament/'.$row["t_id"].'">';
						echo '<button type="button" class="btn btn-info btn-sm">Muokkaa</button>';
					echo '</a></td>';
					echo '<td><a href="'.getBasePath().'/deletetournament/'.$row["t_id"].'">';
						echo '<button type="button" class="btn btn-primary btn-sm">Poista</button>';
					echo '</a></td></tr>';
		}
	echo '</table>';
?>