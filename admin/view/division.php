<?php

if (isset($_POST['homescore']))
{
	$GLOBALS['database']->updateScorePlusOne($_POST['scoreid'],"homescore");
}
else if (isset($_POST['visitorscore']))
{
	$GLOBALS['database']->updateScorePlusOne($_POST['scoreid'],"visitorscore");
}

echo '<h2>Joukkueet</h2>';
	echo '<table class="table table-bordered-games">';
		echo '<tr><th>Nimi</th><th>Voitot</th><th>Tasapelit</th><th>Tappiot</th><th>Muokkaa</th><th>Poista</th>';
		echo '</tr>';
		foreach($GLOBALS['database']->getTeams($command[1]) as $row) 
		{
			echo '<tr><td>'.$row["team_name"].'</td>
				<td>'.$row["team_win"].'</td>
				<td>'.$row["team_tie"].'</td>
				<td>'.$row["team_lose"].'</td>';
				
				echo '<td><a href="'.getBasePath().'/editteam/'.$row["team_id"].'"><button type="button" class="btn btn-info btn-xs">Muokkaa</button> </a></td>';
				echo '</td>
				<td><a href="'.getBasePath().'/deleteteam/'.$row["team_id"].'"><button type="button" class="btn btn-primary btn-xs">Poista</button> </a></td>';
				echo '</tr>';
		}
		
	echo '</table>';

echo '<h2>Ottelut</h2>';
	echo '<table class="table table-bordered-games">';
		echo '<tr><th>Aika</th><th>Kenttä</th><th>Koti</th><th>Vieras</th><th>Tulos</th><th>Muokkaa</th><th>Poista</th>';
		//echo '<th colspan="2">Lisää</th>';
		echo '</tr>';
		foreach($GLOBALS['database']->getGames($command[1]) as $row) 
		{
			echo '<tr><td>'.date('j.n.Y H:i',strtotime($row["game_time"])).'</td>
				<td>'.$row["field_name"].'</td>
				<td>'.$row["team1_name"].'</td>
				<td>'.$row["team2_name"].'</td>
				<td>';
				
				$scores = $GLOBALS['database']->getAllGameScores($row["game_id"])->fetchall();
				end($scores);
				$lastkey = key($scores);
				foreach($scores as $key => $score){
						if($score["score_team1"] != null){echo $score["score_team1"];}else{echo 'x';}
						echo '-';
						if($score["score_team2"] != null){echo $score["score_team2"];}else{echo 'x';}
					if($key != $lastkey)
					{
						echo ', ';
					}
				}
				echo '</td>
				<td><a href="'.getBasePath().'/editmatch/'.$row["game_id"].'"><button type="button" class="btn btn-info btn-xs">Muokkaa</button> </a></td>';
				echo '<td><a href="'.getBasePath().'/deletematch/'.$row["game_id"].'"><button type="button" class="btn btn-primary btn-xs">Poista</button> </a></td>';
				/*
				echo'
				<td>
				<form action="" method="post">
				<input type="hidden" name="scoreid" value="'. $row['score_id'].'">
				<button type="submit" name="homescore" class="btn btn-info btn-xs">Koti+1</button>
				</form>
				</td>
				<td>
				<form action="" method="post">
				<input type="hidden" name="scoreid" value="'. $row['score_id'].'">
				<button type="submit" name="visitorscore" class="btn btn-info btn-xs">Vieras+1</button>
				</form>
				</td>';
				*/
				echo '</tr>';
		}
		
	echo '</table>';
	
?>