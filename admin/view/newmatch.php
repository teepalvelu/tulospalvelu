<?php
if(isset($_POST['newmatch'])){
	if($_POST['field'] != "" && $_POST['time'] != "" && $_POST['team1'] != "" && $_POST['team2'] != ""){
		if ($_POST['team1'] == $_POST['team2'])
		{
			$GLOBALS['error']->setError("Joukkue ei voi pelata itseään vastaan!");
		}
		else{
			$GLOBALS['database']->addNewGame($_POST['time'], $_POST['team1'], $_POST['team2'], $_POST['field']);
		}
	}else{
		$GLOBALS['error']->setError("Otteluaika tai joku muu tieto puuttui");
	}
}

if($GLOBALS['database']->getTeamCount($command[1]) < 2){
	 $GLOBALS['error']->setError("Lohkossa tulee olla vähintään kaksi joukkuetta.");	 
}
else {

	 echo '<div style="text-align: center;">';
			echo '<form action="" method="post">';
			echo '<h2>Lisää ottelu</h2>';
				echo '<table style="margin: 2px auto;">';
					echo '<tr><td>Otteluaika: </td><td><input type="datetime" name="time"></td></tr>';
					echo '<tr><td>Kotijoukkue: </td><td><select name="team1">';
						 foreach($GLOBALS['database']->getTeams($command[1]) as $row)
							 {
							 echo '<option value="'.$row['team_id'].'">' . $row['team_name'] . '</option>';
							 }
					echo '</select>';
					
					echo '<td>Vierasjoukkue: </td><td><select name="team2">';
						 foreach($GLOBALS['database']->getTeams($command[1]) as $row)
							 {
							 echo '<option value="'.$row['team_id'].'">' . $row['team_name'] . '</option>';
							 }
					echo '</select></tr>';
					echo '<tr><td>Kenttä: </td><td><select name="field">';
						 foreach($GLOBALS['database']->getFields() as $row)
							 {
							 echo '<option value="'.$row['field_id'].'">' . $row['field_name'] . '</option>';
							 }	
					echo '</select></tr>';
					echo '<tr><td></td><td><input type="submit" class="btn-info" name="newmatch" value="Lisää"></td></tr>';
				echo '</table>';
			echo '</form>';
			echo '</div>';
   }
?>