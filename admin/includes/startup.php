<?php
require_once ('../includes/settings.php');
require_once ('../class/Settings.class.php');
require_once ('./class/Errors.class.php');
require_once ('./class/AdminDatabaseController.class.php');
require_once ('./class/AdminUserController.class.php');


Settings::setProtected('db_hostname', $db_host);
Settings::setProtected('db_username', $db_user);
Settings::setProtected('db_password', $db_pass);
Settings::setProtected('db_database', $db_name);

$GLOBALS['database'] = new DatabaseController;
$GLOBALS['admin'] = new AdminUserController;
$GLOBALS['error'] = new Errors;
?>