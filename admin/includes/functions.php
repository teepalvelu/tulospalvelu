<?php
//Virheviestien tulostus
	function parseErrors($errorlist){
		foreach($errorlist as $message){
			echo '<p>'.$message.'</p>';
		}
	}

//Suorituskansion selvittäminen webbiserverillä
	function getBasePath(){
	$requestURI = explode("/", $_SERVER["REQUEST_URI"]);
	$scriptName = explode("/",$_SERVER["SCRIPT_NAME"]);
	$basepath = "";
	for($i= 0;$i < sizeof($scriptName);$i++)
			{
		  if ($requestURI[$i] == $scriptName[$i] && $requestURI[$i] != "")
				  {
					$basepath .= "/".$scriptName[$i];
				}
		  }
	//Palautetaan polku, esim http://teepalvelu.dy.fi/tulospalvelu palauttaa /tulospalvelu
	return $basepath;
	}

//Haetaan kenoviivalla "komennot" URL osoitteesta. (Routing)
	function getCommands(){
	$requestURI = explode("/", $_SERVER["REQUEST_URI"]);
	$scriptName = explode("/",$_SERVER["SCRIPT_NAME"]);
	 
	for($i= 0;$i < sizeof($scriptName);$i++)
			{
		  if ($requestURI[$i] == $scriptName[$i])
				  {
					unset($requestURI[$i]);
				}
		  }
	return array_values($requestURI);
	}

//Sisällön näyttäminen
	function showContent(){
		//Jos kirjauduttiin, tarkistetaan username / password
		if(isset($_POST['login'])){
			$GLOBALS['admin']->checkLogin($_POST['username'], $_POST['password']);
		}
		
		//Jos kirjauduttiin ulos, tuhotaan sessio ja ohjataan etusivulle
		if(isset($_POST['logout'])){
			session_destroy();
			header('Location: '.getBasepath().'');
		}
	
		//ROUTING
		$command = getCommands();
		//Jos urlissa ei ollut commandeja, asetetaan tornamentlist
		if($command[0] == ""){$command[0] = "tournamentlist";} 
		@$tournament_id = $command[1];
		//END ROUTING
	
			if($GLOBALS['admin']->isLoggedIn()){ //Tarkistetaan onko kirjauduttu sisään
			
			/*Vasen column (menu)*/
			echo '<div id="col-left">';
				if(!@include('view/menu/'.$command[0].'.php')){
					echo 'Empty left div';
				}
				showLogout();
			echo '</div>';
			/**/
			
			/*Oikea column (admincontent)*/
			echo '<div id="col-right">';
				if(!@include('view/'.$command[0].'.php')){
					echo 'Empty right div';
				}
			if((count($GLOBALS['error']->getErrorlist()))) {?>
				<div class="alert alert-dismissable alert-danger">
					<?php parseErrors($GLOBALS['error']->getErrorlist()); ?>
				</div>
			<?php }
			if((count($GLOBALS['error']->getMsglist()))) { ?>
				<div class="alert alert-dismissable alert-success">
					<?php parseErrors($GLOBALS['error']->getMsglist()); ?>
				</div>
			<?php }				
			echo '</div>';
			/**/
	
			}else{ //Jos ei oltu kirjauduttu sisään
				showLogin(); //Näytetään login laatikko
				if((count($GLOBALS['error']->getErrorlist()))) {?>
				<div class="alert alert-dismissable alert-danger">
					<?php parseErrors($GLOBALS['error']->getErrorlist()); ?>
				</div>
			<?php }
			}	
	}
//Login laatikon näyttäminen
	function showLogin(){
		echo '<div style="text-align: center;">';
		echo '<form action="" method="post">';
		echo '<h2>Login (admin/pw)</h2>';
			echo '<table style="margin: 0 auto;">';
				echo '<tr><td>Username:</td><td><input type="text" name="username"></td></tr>';
				echo '<tr><td>Password:</td><td><input type="password" name="password"></td></tr>';
				echo '<tr><td></td><td><input type="submit" name="login" value="login"></td></tr>';
			echo '</table>';
		echo '</form>';
		echo '</div>';
	}

//Login napin näyttäminen
	function showLogout(){
		echo '<form action="" method="post">';
			echo '<input type="submit" class="btn btn-default" style="width: 100%;" name="logout" value="Logout">';
		echo '</form>';
	}
	
//Levänmurujen selvittäjä admin puolelle
	function resolver($type, $id){
		switch($type){
			case "league":
			case "newdivision":
			case "editleague":
			case "deleteleague":
				foreach ($GLOBALS["database"]->leagueResolver($id) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]),
							"league" =>  array("id" => $data["league_id"], "name" => $data["league_name"]));
				}
				break;
			case "division":
			case "editdivision":
			case "deletedivision":
			case "newteam":
			case "newmatch":
				foreach ($GLOBALS["database"]->divisionResolver($id) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]),
							"league" =>  array("id" => $data["league_id"], "name" => $data["league_name"]),
							"division" =>  array("id" => $data["division_id"], "name" => $data["division_name"]));
				}
			break;
			case "editteam":
			case "deleteteam":
			$teamDivision = $GLOBALS["database"]->teamDivResolver($id)->fetch();
				foreach ($GLOBALS["database"]->divisionResolver($teamDivision['division_id']) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]),
							"league" =>  array("id" => $data["league_id"], "name" => $data["league_name"]),
							"division" =>  array("id" => $data["division_id"], "name" => $data["division_name"]));
				}
			break;
			case "editmatch":
			case "deletematch":
			$matchDivision = $GLOBALS["database"]->gameDivResolver($id)->fetch();
				foreach ($GLOBALS["database"]->divisionResolver($matchDivision['division_id']) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]),
							"league" =>  array("id" => $data["league_id"], "name" => $data["league_name"]),
							"division" =>  array("id" => $data["division_id"], "name" => $data["division_name"]));
				}
			break;			
			case "tournament":
			case "edittournament":
			case "deletetournament":
			case "newleague":
			case "scorecards":
				foreach ($GLOBALS["database"]->tournamentResolver($id) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]));
				}
			break;
		}
	}
?>