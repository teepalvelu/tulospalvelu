<?php
function getGames()
	{
	    include("../../../includes/settings.php");		
		try
		{
			$db = new PDO('mysql:host=' . $db_host . ';dbname=' . $db_name . ';charset=utf8', $db_user, $db_pass);
		} 
		catch(PDOException $e)
		{
			Die("Error");
		}
	
		$postleagues = $_POST["leagues"];
		$leaguearray = array();
		foreach($postleagues as $league){
		array_push($leaguearray,(int)$league);
		}

		$query = 'SELECT
				game.game_id as game_id,
				game.game_verification as game_verification,
				game.game_time as game_time,
				team1.team_name as team1_name,
				team2.team_name as team2_name,
				league.league_name as league_name,
				division.division_name as division_name,
				tournament.tournament_name as tournament_name,
				field.field_name as field_name
			FROM
				game AS game
			LEFT JOIN
				team AS team1
				ON team1.team_id = game.game_team1
			LEFT JOIN
				team AS team2
				ON team2.team_id = game.game_team2
			LEFT JOIN 
				division as division
				ON team2.division_id = division.division_id
			LEFT JOIN
				league as league
				ON league.league_id = division.league_id
			LEFT JOIN
				tournament as tournament
				ON tournament.tournament_id = league.tournament_id
			LEFT JOIN
				field AS field
				ON game.field_id = field.field_id
			LEFT JOIN
				score AS score
				ON game.game_id = score.game_id
			WHERE league.league_id IN ('.implode(',', array_map('intval', $leaguearray)).')
			GROUP BY game.game_id
			ORDER BY field.field_id ASC, game.game_timestamp ASC';
			
		$pre = $db->prepare($query);
		$pre->execute();
		return $pre;
	}

define('FPDF_FONTPATH', 'font/');
include_once('ufpdf.php');
//VASEMMALTA, YLHÄÄLTÄ
$pdf=new FPDF('P','mm','A4');

foreach (getGames() as $game) {
$pdf->AddPage();
$pdf->Image('templates/pesapallo.jpg',0,0,210);

$pdf->SetFont('Arial','',9);
//$pdf->Text(8,23,utf8_decode($game["tournament_name"]));
$pdf->Text(9,24,utf8_decode($game["league_name"]));
$pdf->Text(9,27,utf8_decode($game["division_name"]));
//$pdf->Text(115,16,$info);

$pdf->SetFont('Arial','',7);
$pdf->Text(57,24,date('d.m.Y H:i',strtotime($game["game_time"])));
$pdf->Text(57,27,utf8_decode($game["field_name"]));

$pdf->SetFont('Arial','B',12);
$pdf->Text(10,45,utf8_decode($game["team1_name"]));
$pdf->Text(110,45,utf8_decode($game["team2_name"]));

$pdf->SetFont('Arial','B',14);
$pdf->Text(115,280,$game["game_id"].$game["game_verification"]);
}
$pdf->Output('kirja.pdf','D');
?>