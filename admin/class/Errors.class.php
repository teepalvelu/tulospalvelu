<?php
class Errors
{
    private $_errorlist; 
	private $_msglist;

	function __construct() {
		$this->_errorlist = array();
		$this->_msglist = array();
	}
	
	function __destruct(){
		$this->_errorlist = null;
		$this->_msglist = null;
	}
	
    function getErrorlist()
    {	
        return $this->_errorlist;
    }

    function setError($message)
    {
        array_push($this->_errorlist, $message);
    }
	
	function getMsglist()
    {	
        return $this->_msglist;
    }

    function setMsg($message)
    {
        array_push($this->_msglist, $message);
    }
}
?>