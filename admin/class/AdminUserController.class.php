<?php
class AdminUserController{
	function isAdmin(){
		return true;
	}
	
	function isGlobalAdmin($userid){
		return true;
	}
	
	function isLoggedIn(){
		return isset($_SESSION['userid']);
	}
	
	function permissionTournament($tournament_id){
		return true;
	}
	
	function checkLogin($username, $password){
		$userid = $GLOBALS['database']->LoginGetUserid($username,$password);
		if($userid){
			$_SESSION['userid'] = $userid;
		}else{
			//echo '<div class="alert alert-dismissable alert-danger">';
			//echo 'Login failed!';
			//echo '</div>';
			$GLOBALS['error']->setError("Login failed");
		}
	}
	
	function checkPermission($tournament_id){
		$permission_array = unserialize($GLOBALS['database']->getPermissionArray($_SESSION['userid']));
		return in_array($tournament_id, $permission_array);
	}
}
?>