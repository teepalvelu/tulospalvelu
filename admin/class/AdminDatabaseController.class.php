<?php
// Luokka admin puolen tietokantakyseleyille

class DatabaseController {
    private $_database;
	private $_db_user;
	private $_db_pass;
	private $_db_host;
	private $_db_name;
	
    function __construct() {
		$this->_db_user = Settings::getProtected('db_username');
		$this->_db_pass = Settings::getProtected('db_password');
		$this->_db_host = Settings::getProtected('db_hostname');
		$this->_db_name = Settings::getProtected('db_database');
		$this->_database = $this->connectDatabase();
    }

    function __destruct() {
		$this->_database = NULL;
    }
	
	//TIETOKANTAAN YHDISTÄMINEN
	function connectDatabase(){
		try{
			return new PDO('mysql:host='.$this->_db_host.';dbname='.$this->_db_name.';charset=utf8',$this->_db_user, $this->_db_pass);
		} catch (PDOException $e)
		{
				//Jos tulee virhe, asetetaan virheviesti Errors luokkaan
				Errors::setError("Sorry, there was a database error. We are sorry!");
		}
	}

	//KIRJAUTUMISEN TARKISTUS
	function LoginGetUserid($username, $password)
		{
			$query = 'SELECT admin_id
						FROM admin
						WHERE admin_name = ? AND admin_password = ?
						LIMIT 1';
			$pre = $this->_database->prepare($query);
			$pre->execute(array($username, md5($password)));
			$result = $pre->fetch();
			return $result["admin_id"]; //Palautetaan userid
		}
	
	function GetPermissionArray($userid){
			$query = 'SELECT admin_permission
						FROM admin
						WHERE admin_id = ?
						LIMIT 1';
			$pre = $this->_database->prepare($query);
			$pre->execute(array($userid));
			$result = $pre->fetch();
			return $result["admin_permission"];
	}
	
	function getUserDetails($userid){
			$query = 'SELECT *
						FROM admin
						WHERE admin_id = ?
						LIMIT 1';
			$pre = $this->_database->prepare($query);
			$pre->execute(array($userid));
			return $pre;
	}
	
	function editUserDetails($username,$firstname,$secondname,$email,$permissions,$password,$userid){
		if($password != ""){
			$query = 'UPDATE admin
				SET admin_name = ?, admin_firstName = ?, admin_secondName = ?, admin_email = ?, admin_password = ?, admin_permission = ?
				WHERE admin_id = ?
				LIMIT 1';
		}else{
				$query = 'UPDATE admin
				SET admin_name = ?, admin_firstName = ?, admin_secondName = ?, admin_email = ?, admin_permission = ?
				WHERE admin_id = ?
				LIMIT 1';
		}
			$pre = $this->_database->prepare($query);
			if($password != ""){
			$pre->execute(array($username,$firstname,$secondname,$email,$password,$permissions,$userid));
			}else{
			$pre->execute(array($username,$firstname,$secondname,$email,$permissions,$userid));
			}
	}
	
	function addUser($username,$firstname,$secondname,$email,$permissions,$password){
			$query = 'INSERT INTO admin
				(admin_name, admin_firstName, admin_secondName, admin_email, admin_password, admin_permission)
				VALUES
				(?,?,?,?,?,?)';
			$pre = $this->_database->prepare($query);
			$pre->execute(array($username,$firstname,$secondname,$email,$password,$permissions));
	}
	
	function getAllUsers() {
		$query = 'SELECT * FROM admin';
		$pre = $this->_database->prepare($query);
		$pre->execute();
		return $pre;
	}
	
		//TURNAUSLISTAN HAKU
	function getTournaments(){
		$query = 'SELECT 
					tournament.tournament_id as t_id,
					tournament.tournament_name as t_name,
					tournament.tournament_time as t_time,
					COUNT(league.league_id) as leagues
				FROM
					tournament
				LEFT OUTER JOIN
					league 
				ON league.tournament_id = tournament.tournament_id
				GROUP BY tournament.tournament_id
				ORDER BY tournament.tournament_time DESC';
		$pre = $this->_database->prepare($query);
		$pre->execute();
		return $pre;
	}
	
	//SARJALISTAN HAKU
	function getLeagues($tid){
		$query = 'SELECT 
					league.league_id as l_id,
					league.league_name as l_name,
					COUNT(division.division_id) as divisions
				FROM
					league
				LEFT OUTER JOIN
					division 
				ON division.league_id = league.league_id
				WHERE league.tournament_id = ?
				GROUP BY league.league_id';
		$pre = $this->_database->prepare($query);
		$pre->execute(array($tid));
		return $pre;
	}
	
	//LOHKOLISTAN HAKU
	function getDivisions($did){
		$query = 'SELECT 
					division.division_id as d_id,
					division.division_name as d_name,
					COUNT(team.team_id) as teams
				FROM
					division
				LEFT OUTER JOIN
					team 
				ON team.division_id = division.division_id
				WHERE division.league_id = ?
				GROUP BY division.division_id';
		$pre = $this->_database->prepare($query);
		$pre->execute(array($did));
		return $pre;
	}
	
	function getDivisionById($did){
			$query = 'SELECT * FROM division WHERE division_id = ? LIMIT 1';
		$pre = $this->_database->prepare($query);
		$pre->execute(array($did));
		return $pre;
	}
	
	function getTeamCount($did){
	$query = 'SELECT
				COUNT(*)
			FROM team
				WHERE team.division_id = ?';
	
		$pre = $this->_database->prepare($query);
		$pre->execute(array($did));
		$teamcount = $pre->fetch(PDO::FETCH_NUM);
		
		return $teamcount[0];
	}
	
	function getScore($gid, $part){
	$query = 'SELECT
				score_team1,
				score_team2,
				score_part
			FROM score
				WHERE game_id = ?
				AND score_part = ?
			LIMIT 1';
	
		$pre = $this->_database->prepare($query);
		$pre->execute(array($gid, $part));
		return $pre;
	}
	
	function getAllGameScores($gid){
	$query = 'SELECT
				score_team1,
				score_team2
			FROM score
				WHERE game_id = ?
			ORDER BY score_part ASC';
	
		$pre = $this->_database->prepare($query);
		$pre->execute(array($gid));
		return $pre;
	}
	
	function getTeams($did)
	{
	$query = 'SELECT
				*
			FROM team
				WHERE team.division_id = ?';
	
		$pre = $this->_database->prepare($query);
		$pre->execute(array($did));
		return $pre;
	}
	
	function getTeam($tid)
	{
		$query = 'SELECT * FROM team WHERE team_id = ? LIMIT 1';
		$pre = $this->_database->prepare($query);
		$pre->execute(array($tid));
		return $pre;
	}
	
	function getFields()
	{
		$query = 'SELECT * FROM field';
		$pre = $this->_database->prepare($query);
		$pre->execute();
		return $pre;
	}
	
	function getSettings($id)
	{
		$query = 'SELECT * FROM setting WHERE setting_id = ? LIMIT 1';
		$pre = $this->_database->prepare($query);
		$pre->execute(array($id));
		return $pre;
	}
	
			//OTTELUIDEN HAKU
	function getGames($did)
	{
		$query = 'SELECT 
				game.game_id as game_id,
				score.score_id as score_id,
				game.game_time as game_time,
				team1.team_name as team1_name,
				team2.team_name as team2_name,
				field.field_name as field_name,
				SUM(score.score_team1) as score_team1,
				SUM(score.score_team2) as score_team2
			FROM
				game AS game
			LEFT JOIN
				team AS team1
				ON team1.team_id = game.game_team1
			LEFT JOIN
				team AS team2
				ON team2.team_id = game.game_team2
			LEFT JOIN
				field AS field
				ON game.field_id = field.field_id
			LEFT JOIN
				score AS score
				ON game.game_id = score.game_id
			WHERE team1.division_id = ? OR team2.division_id = ?
			GROUP BY game.game_id
			ORDER BY game.game_time ASC';
				
			$pre = $this->_database->prepare($query);
			$pre->execute(array($did,$did));
			return $pre;
	}
	
	function getSingleGameById($gid) {
	$query = 'SELECT 
				game.game_id as game_id,
				game.game_state as game_state,
				score.score_id as score_id,
				game.game_time as game_time,
				team1.team_name as team1_name,
				team1.team_id as team1_id,
				team2.team_name as team2_name,
				team2.team_id as team2_id,
				team2.division_id as division_id,
				field.field_name as field_name,
				field.field_id as field_id,
				SUM(score.score_team1) as score_team1,
				SUM(score.score_team2) as score_team2
			FROM
				game AS game
			LEFT JOIN
				team AS team1
				ON team1.team_id = game.game_team1
			LEFT JOIN
				team AS team2
				ON team2.team_id = game.game_team2
			LEFT JOIN
				field AS field
				ON game.field_id = field.field_id
			LEFT JOIN
				score AS score
				ON game.game_id = score.game_id
			WHERE game.game_id = ?
			GROUP BY game.game_id';
		
		$pre = $this->_database->prepare($query);
		$pre->execute(array($gid));
		return $pre;
	}
	
	function addNewTournament($name, $date){
		$query = 'INSERT INTO tournament (tournament_name, tournament_time)
					VALUES(?, ?)';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $date)))
		{
			$GLOBALS['error']->setMsg("Turnaus lisättiin");
		}
		else
		{
			$GLOBALS['error']->setError("Adding new tournament failed.");
		}
	}
	
	function editTournament($name, $date, $tid){
		$query = "UPDATE tournament SET tournament_name = ?, tournament_time = ? WHERE tournament_id = ? LIMIT 1";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $date, $tid)))
		{
			$GLOBALS['error']->setMsg("Muokkaukset tallennettu");
		}
		else
		{
			$GLOBALS['error']->setError("Muokkaus epäonnitui");
		}
	}
	
	function editLeague($name, $lid){
		$query = "UPDATE league SET league_name = ? WHERE league_id = ? LIMIT 1";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $lid)))
		{
			$GLOBALS['error']->setMsg("Muokkaukset tallennettu");
		}
		else
		{
			$GLOBALS['error']->setError("Muokkaus epäonnitui");
		}
	}
	
	function editDivision($name, $did){
		$query = "UPDATE division SET division_name = ? WHERE division_id = ? LIMIT 1";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $did)))
		{
			$GLOBALS['error']->setMsg("Muokkaukset tallennettu");
		}
		else
		{
			$GLOBALS['error']->setError("Muokkaus epäonnitui");
		}
	}
	
	function editField($name, $location, $fid){
		$query = "UPDATE field SET field_name = ?, field_location = ? WHERE field_id = ? LIMIT 1";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $location, $fid)))
		{
			$GLOBALS['error']->setMsg("Muokkaukset tallennettu");
		}
		else
		{
			$GLOBALS['error']->setError("Muokkaus epäonnitui");
		}	
	}
	
	function editTeam($name, $tid){
		$query = "UPDATE team SET team_name = ? WHERE team_id = ? LIMIT 1";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $tid)))
		{
			$GLOBALS['error']->setMsg("Muokkaukset tallennettu");
		}
		else
		{
			$GLOBALS['error']->setError("Muokkaus epäonnitui");
		}	
	}
	
	function editSettings($gamewinpt, $gametiept, $gamelosept, $gametype, $did) {
		$query = 'UPDATE setting SET setting_gameWinPt = ?, setting_gameTiePt = ?, 
		setting_gameLosePt = ?, setting_gameType = ? WHERE setting_id = ? LIMIT 1';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($gamewinpt, $gametiept, $gamelosept, $gametype, $did)))
		{
			$GLOBALS['error']->setMsg("Asetusten muokkaus onnistui");
		}
		else
		{
			$GLOBALS['error']->setError("Kysely epäonnistui");
		}
	}
	
	function updateMatch($gid, $state, $home, $visitor, $field, $time)
	{
		$query = "UPDATE game SET game_state = ?, game_team1 = ?, game_team2 = ?, field_id = ?, game_time = ? WHERE game_id = ? LIMIT 1";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($state, $home, $visitor, $field, $time, $gid)))
		{
			$GLOBALS['error']->setMsg("Muokkaukset tallennettu");
		}
		else
		{
			$GLOBALS['error']->setError("Muokkaus epäonnitui");
		}	
	}
	
	function updateScore($gid, $score_home, $score_away, $part){
		$query = "INSERT INTO score (game_id, score_team1, score_team2, score_part) VALUES (?,?,?,?)
					ON DUPLICATE KEY
				UPDATE score_team1 = ?, score_team2 = ?";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($gid, $score_home, $score_away, $part, $score_home, $score_away))){
			$GLOBALS['error']->setMsg("Tulosrivi päivitetty");
		}else{
			$GLOBALS['error']->setError("Tulosrivin päivitys epäonnistui");
		}
	}
	
	function updateScorePlusOne($sid, $team){
		if ($team == "homescore")
		{
			$query = "UPDATE score SET score_team1 = score_team1 + 1 WHERE score_id = ?";
		}
		else if ($team == "visitorscore")
		{
			$query = "UPDATE score SET score_team2 = score_team2 + 1 WHERE score_id = ?";
		}
	
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($sid)))
		{
			$GLOBALS['error']->setMsg("Tulosta päivitetty");
		}
		else
		{
			$GLOBALS['error']->setError("Tuloksen päivitys epäonnistui");
		}
	}
	
	function addNewLeague($tournamentid, $name) {
		$query  = 'INSERT INTO league (tournament_id, league_name)
					VALUES(?, ?)';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($tournamentid, $name)))
		{
			$GLOBALS['error']->setMsg('<p> Sarjan "' . $name . '" lisääminen onnistui </p>');
		}
		else
		{
			$GLOBALS['error']->setError("Adding new league failed.");
		}
	
	}
	
	function addNewDivision($name, $leagueid) {
		$query = 'INSERT INTO setting VALUE ()';
		$pre = $this->_database->prepare($query);
		if($pre->execute())
		{
			$GLOBALS['error']->setMsg("Asetusten lisäys onnistui");
		}
		else
		{
			$GLOBALS['error']->setError("Kysely epäonnistui");
		}
		
		$settingid = $this->_database->lastInsertId();
		
		$query = 'INSERT INTO division(division_name, league_id, division_setting_id)
					VALUES(?, ?, ?)';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $leagueid, $settingid)))
		//0 = $settingid
		{
			$GLOBALS['error']->setMsg("Lohko lisättiin onnistuneesti.");
		}
		else
		{
			$GLOBALS['error']->setError("Adding new division failed.");
		}
	}

	
	function addNewTeam($name, $divisionid, $teamwin, $teamtie, $teamlose) {
		$query = 'INSERT INTO team(team_name, division_id, team_win, team_tie, team_lose)
					VALUES(?,?,?,?,?)';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $divisionid, $teamwin, $teamtie, $teamlose)))
		{
			$GLOBALS['error']->setMsg("Joukkue lisätty.");
		}
		else
		{
			$GLOBALS['error']->setError("Adding new team failed.");
		}	
	}
	
	function addNewField($name, $location) {
		$query = 'INSERT INTO field(field_name, field_location)
					VALUES(?,?)';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($name, $location)))
		{
			$GLOBALS['error']->setMsg("Kenttä lisättiin");
		}
		else
		{
			$GLOBALS['error']->setError("Adding new field failed.");
		}				
	}
	
	function addNewScore($gameid, $score_team1, $score_team2, $score_part) {
		$query = 'INSERT INTO score(game_id, score_team1, score_team2, score_part)
				VALUES(?,?,?,?)';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($gameid, $score_team1, $score_team2, $score_part)))
		{
			$GLOBALS['error']->setMsg("Tuloksen lisäys onnistui");
		}
		else
		{
			$GLOBALS['error']->setError("Adding new score failed.");
		}			
	}

	function addNewGame($gametime, $team1, $team2, $field_id) {
		$query = 'INSERT INTO game(game_time, game_team1, game_team2, field_id, game_state, game_verification)
					VALUES(?,?,?,?,?,?)';
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($gametime, $team1, $team2, $field_id, 0, rand(10,99))))
		{
			$GLOBALS['error']->setMsg("Ottelu lisätty");
			$currentid = $this->_database->lastInsertId();
			$this->addNewScore($currentid, 0, 0, 1);
		}
		else
		{
			$GLOBALS['error']->setError("Adding new game failed.");
		}
	}
	
	function deleteField($fid) {
	
		$cnt = $this->_database->prepare('SELECT COUNT(*) as lkm FROM games WHERE field_id = ?;');
		$cnt->execute(array($fid));
	
		$cntresult = $cnt->fetch();
	
		if ($cntresult['lkm'] == 0)
		 {
			$query = "DELETE FROM field WHERE field_id = ?";
			$pre = $this->_database->prepare($query);
			if($pre->execute(array($fid)))
			{
				$GLOBALS['error']->setMsg("Poisto onnistui");
			}
			else
			{
				$GLOBALS['error']->setError("Poisto epäonnistui");
			}	
		}
		else
		{
				$GLOBALS['error']->setError("Kentällä ei saa olla pelejä");
		}
	}

	function deleteTournament($tid) {
	
		$cnt = $this->_database->prepare('SELECT COUNT(*) as lkm FROM league WHERE tournament_id = ?;');
		$cnt->execute(array($tid));
	
		$cntresult = $cnt->fetch();
	
		if ($cntresult['lkm'] == 0)
		 {
			$query = "DELETE FROM tournament WHERE tournament_id = ?";
			$pre = $this->_database->prepare($query);
			if($pre->execute(array($tid)))
			{
				$GLOBALS['error']->setMsg("Poisto onnistui");
			}
			else
			{
				$GLOBALS['error']->setError("Poisto epäonnistui");
			}	
		}
		else
		{
				$GLOBALS['error']->setError("Turnauksessa ei saa olla sisältöä!");
		}
	}

		function deleteDivision($did) {
	
		$cntteam = $this->_database->prepare('SELECT COUNT(*) as lkm FROM team WHERE division_id = ?;');
		$cntteam->execute(array($did));
	
		$cntteamresult = $cntteam->fetch();
	
		if ($cntteamresult['lkm'] != 0)
		{
			$GLOBALS['error']->setError("Lohkossa ei saa olla joukkueita!");
			return;
		}
		$cntmatch = $this->_database->prepare('SELECT COUNT(*) as lkm 
				FROM team 
				INNER JOIN game
				ON game.game_team1 = team.team_id OR game.game_team2 = team.team_id
				WHERE team.division_id = ?;');
		$cntmatch->execute(array($did));
		$cntmatchresult = $cntmatch->fetch();
	
		if ($cntmatchresult['lkm'] != 0)
		{
			$GLOBALS['error']->setError("Lohkossa ei saa olla otteluita!");
			return;
		}
		 
		$query = "DELETE FROM division WHERE division_id = ?";
		$pre = $this->_database->prepare($query);
		if($pre->execute(array($did)))
		{
			$GLOBALS['error']->setMsg("Poisto onnistui");
		}
		else
		{
			$GLOBALS['error']->setError("Poisto epäonnistui");
		}	
	}
	
	function deleteLeague($lid) {
	
		$cnt = $this->_database->prepare('SELECT COUNT(*) as lkm FROM division WHERE league_id = ?;');
		$cnt->execute(array($lid));
	
		$cntresult = $cnt->fetch();
	
		if ($cntresult['lkm'] == 0)
		 {
			$query = "DELETE FROM league WHERE league_id = ?";
			$pre = $this->_database->prepare($query);
			if($pre->execute(array($lid)))
			{
				$GLOBALS['error']->setMsg("Poisto onnistui");
			}
			else
			{
				$GLOBALS['error']->setError("Poisto epäonnistui");
			}	
		}
		else
		{
				$GLOBALS['error']->setError("Sarjassa ei saa olla sisältöä!");
		}
	}
	
	function deleteTeam($tid) {
	
		$cnt = $this->_database->prepare('SELECT COUNT(*) as lkm FROM game WHERE game_team1 = ? OR game_team2 = ?;');
		$cnt->execute(array($tid,$tid));
	
		$cntresult = $cnt->fetch();
	
		if ($cntresult['lkm'] == 0)
		 {
			$query = "DELETE FROM team WHERE team_id = ?";
			$pre = $this->_database->prepare($query);
			if($pre->execute(array($tid)))
			{
				$GLOBALS['error']->setMsg("Poisto onnistui");
			}
			else
			{
				$GLOBALS['error']->setError("Poisto epäonnistui");
			}	
		}
		else
		{
				$GLOBALS['error']->setError("Joukkueella ei saa olla pelejä!");
		}
	}
	
	function deleteGame($gid) {
			$query = "DELETE FROM game WHERE game_id = ?";
			$pre = $this->_database->prepare($query);
			if($pre->execute(array($gid)))
			{
				$GLOBALS['error']->setMsg("Poisto onnistui");
			}
			else
			{
				$GLOBALS['error']->setError("Poisto epäonnistui");
			}	
		}		
	
		//Divistä ylöispäin olevan datan haku
	function divisionResolver($did) 
	{
		$query = 'SELECT d.division_id, d.division_name, d.league_id, l.league_name, l.tournament_id, t.tournament_name
				FROM division as d LEFT JOIN league as l
				ON l.league_id = d.league_id
				LEFT JOIN tournament as t
				ON l.tournament_id = t.tournament_id
				WHERE division_id = ?;';
				
		$pre = $this->_database->prepare($query);
		$pre->execute(array($did));
		return $pre;
	}
	
	function leagueResolver($lid) 
	{
		$query = 'SELECT l.league_id, l.league_name, l.tournament_id, t.tournament_name
		FROM league as l
		LEFT JOIN tournament as t
		ON l.tournament_id = t.tournament_id
		WHERE league_id = ?;';
				
		$pre = $this->_database->prepare($query);
		$pre->execute(array($lid));
		return $pre;
	}
	
	function tournamentResolver($tid) 
	{
		$query = 'SELECT tournament_id, tournament_name, tournament_time
		FROM
		tournament
		WHERE tournament_id = ?;';
				
		$pre = $this->_database->prepare($query);
		$pre->execute(array($tid));
		return $pre;
	}
	
	function fieldResolver($fid)
	{
		$query = 'SELECT field_id, field_name, field_location
		FROM
		field
		WHERE field_id = ?;';
				
		$pre = $this->_database->prepare($query);
		$pre->execute(array($fid));
		return $pre;		
	}
	
	function teamDivResolver($tid)
	{
		$query = 'SELECT division_id
		FROM
		team
		WHERE team_id = ?;';
				
		$pre = $this->_database->prepare($query);
		$pre->execute(array($tid));
		return $pre;
	}
	
	function gameDivResolver($gid)
	{
		$query = 'SELECT division.division_id
		FROM
		division
		INNER JOIN team ON team.division_id = division.division_id
		INNER JOIN game on game.game_team1 = team.team_id
		WHERE game.game_id = ?;';
				
		$pre = $this->_database->prepare($query);
		$pre->execute(array($gid));
		return $pre;
	}	
}
?>