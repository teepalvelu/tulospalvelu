-- Kun workbenchilla importtaa tietokannan, asetetaan automaattisesti aluksi jotain siirtoa 
-- nopeuttavia ja helpottavia flageja
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0; -- InnoDB ei tarkasta duplikaattiavaimia, eli importtaus nopeutuu
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0; -- Tarkastetaanko viiteavainten eheys, oltava poissa p��lt� taulua luotaessa
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'; -- Serveri toimii rajaavammassa modessa. Lis�ksi p�iv�m��ri� ei validoida 

--KANNAN LUOMINEN
-- Luodaan taulu turnauksia varten.

CREATE  TABLE IF NOT EXISTS `tournament` (
  `tournament_id` INT NOT NULL AUTO_INCREMENT , -- turnauksen ID ei saa olla Null, ja oletuksena sit� lis�t��n aina yhdell�
  `tournament_name` VARCHAR(200) NULL , -- turnauksen nimi
  `tournament_time` DATETIME NULL , -- turnauksen kellonaika p�iv�m��r�ll�
  PRIMARY KEY (`tournament_id`) ) ;-- turnauksen ID on perusavain


-- luodaan taulu sarjoja varten.

CREATE  TABLE IF NOT EXISTS `league` (
  `league_id` INT NOT NULL AUTO_INCREMENT , -- sarjan ID
  `tournament_id` INT NOT NULL , -- Viiteavain turnausta varten
  `league_name` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`league_id`) , -- sarjan uniikki ID on perusavain
  INDEX `fk_division_tournament_idx` (`tournament_id` ASC) , -- luodaan indeksi tournament id:teit� varten. 
  CONSTRAINT `fk_division_tournament`
    FOREIGN KEY (`tournament_id` )
    REFERENCES `tournament` (`tournament_id` ) -- sarjan viiteavain laitetaan viittaamaan turnaukseen
    ON DELETE NO ACTION -- P�ivityksess� ja poistossa ei suurempia vy�rytyksi� tms.
    ON UPDATE NO ACTION);

 -- Luodaan taulukko pisteasetuksia varten. 
CREATE  TABLE IF NOT EXISTS `setting` (
  `setting_gameWinPt` INT NULL , -- voittokerroin
  `setting_gameTiePt` INT NULL , -- tasapelikerroin
  `setting_gameLosePt` INT NULL , -- h�vi�kerroin
  `setting_gameType` INT NULL DEFAULT 1 , -- Merkkaa erien m��r�� (oletuksena yksi)
  `setting_id` INT NOT NULL AUTO_INCREMENT , -- perusavaimena yksil�iv� ID
  PRIMARY KEY (`setting_id`) );

-- luodaan taulu lohkoja varten
CREATE  TABLE IF NOT EXISTS `division` (
  `division_id` INT NOT NULL AUTO_INCREMENT , -- sarjan ID
  `division_name` VARCHAR(200) NOT NULL , -- lohkon nimi
  `league_id` INT NOT NULL , -- viittaus sarjaan viiteavaimella
  `division_setting_id` INT NOT NULL ,
  PRIMARY KEY (`division_id`) , -- sarjan ID on perusavain
  INDEX `fk_division_league1_idx` (`league_id` ASC) , -- Luodaan indeksit sarjan sek� asetuksen ID:lle.
  INDEX `fk_division_setting1_idx` (`division_setting_id` ASC) ,
  CONSTRAINT `fk_division_league1`
    FOREIGN KEY (`league_id` )
    REFERENCES `league` (`league_id` ) -- lohkon kaksi viiteavainta viittaavat asetuksiin sek� sarjaan. Kummankaan poistossa/p�ivityksess� ei vy�rytyksi�
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_division_setting1`
    FOREIGN KEY (`division_setting_id` )
    REFERENCES `setting` (`setting_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- Luodaan taulu joukkueita varten
CREATE  TABLE IF NOT EXISTS `team` (
  `team_id` INT NOT NULL AUTO_INCREMENT ,
  `team_name` VARCHAR(200) NOT NULL , -- joukkueen nimi
  `division_id` INT NOT NULL ,
  `team_win` INT NULL , -- joukkueen voitot
  `team_tie` INT NULL , -- joukkueen tasapelit
  `team_lose` INT NULL , -- joukkueen tappiot
  PRIMARY KEY (`team_id`) ,
  INDEX `fk_team_division1_idx` (`division_id` ASC) ,
  CONSTRAINT `fk_team_division1` -- joukkeen viiteavain viittaa lohkoon, jossa joukkue "pelaa"
    FOREIGN KEY (`division_id` )
    REFERENCES `division` (`division_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- Luodaan taulu pelikentt�� varten
CREATE  TABLE IF NOT EXISTS `field` (
  `field_id` INT NOT NULL AUTO_INCREMENT ,
  `field_name` VARCHAR(200) NOT NULL , -- kent�n nimi
  `field_location` VARCHAR(200) NULL , -- kent�n sijainti
  PRIMARY KEY (`field_id`) );

  
 -- Luodaan taulu ottelua varten
CREATE  TABLE IF NOT EXISTS `game` (
  `game_id` INT NOT NULL AUTO_INCREMENT ,
  `game_time` DATETIME NULL , -- ottelun kellonaika
  `game_team1` INT NOT NULL , -- ottelun eka joukke
  `game_team2` INT NOT NULL , -- ottelun toka joukke
  `field_id` INT NOT NULL , -- kentt� jossa ottelu pelataaan
  `game_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, -- pelin aikaleima UNIX-aikana
  PRIMARY KEY (`game_id`) ,
  INDEX `fk_game_team1_idx` (`game_team1` ASC) ,
  INDEX `fk_game_team2_idx` (`game_team2` ASC) ,
  INDEX `fk_game_field1_idx` (`field_id` ASC) ,
  CONSTRAINT `fk_game_team1` -- ottelulla on kolme viiteavainta, kaksi jotka viittaavaat pelaaviin joukkeihin, yksi viittaa pelikentt��n
    FOREIGN KEY (`game_team1` )
    REFERENCES `team` (`team_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_team2`
    FOREIGN KEY (`game_team2` )
    REFERENCES `team` (`team_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_field1`
    FOREIGN KEY (`field_id` )
    REFERENCES `field` (`field_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
-- Luodaan taulu yll�pit�j�n tietoja varten.

CREATE  TABLE IF NOT EXISTS `admin` (
  `admin_id` INT NOT NULL AUTO_INCREMENT , -- Kaikki k�ytt�j�n tiedot tulee l�yty�
  `admin_name` VARCHAR(45) NOT NULL ,
  `admin_firstName` VARCHAR(100) NOT NULL ,
  `admin_secondName` VARCHAR(100) NOT NULL ,
  `admin_password` VARCHAR(255) NOT NULL ,
  `admin_email` VARCHAR(255) NOT NULL ,
  `admin_permission` VARCHAR(255) NULL , -- t�h�n tulee serialisoitu PHP-taulukko, jossa on tournament-id:eit� joita kys. yll�pit�j� voi hallinnoida
  PRIMARY KEY (`admin_id`) ,
  UNIQUE INDEX `user_name_UNIQUE` (`admin_name` ASC) , -- Yll�pit�j�n k�ytt�j�tunnuksen tulee olla uniikki
  UNIQUE INDEX `admin_email_UNIQUE` (`admin_email` ASC) );-- Samoin my�s s�hk�postiosoitteen

-- luodaan kanta pisteit� varten
CREATE  TABLE IF NOT EXISTS `score` (
  `score_id` INT NOT NULL AUTO_INCREMENT ,
  `game_id` INT NOT NULL ,
  `score_team1` INT NULL ,
  `score_team2` INT NULL ,
  `score_part` INT NOT NULL , -- v�liaikatulos
  PRIMARY KEY (`score_id`) ,
  INDEX `fk_score_game1_idx` (`game_id` ASC) , -- pisteill� on viiteavain, joka viittaa otteluun
  CONSTRAINT `fk_score_game1`
    FOREIGN KEY (`game_id` )
    REFERENCES `game` (`game_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

	
-- TESTAUSDATA

-- Lis�t��n testidataa tietokantaan, t�ss� tapauksessa kenttiin
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 1", "Paikka 1");
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 2", "Paikka 2");
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 3", "Paikka 3");
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 4", "Paikka 4");
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 5", "Paikka 5");
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 6", "Paikka 6");
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 7", "Paikka 7");
INSERT INTO field (field_name, field_location) VALUES ("Kentt� 8", "Paikka 8");

-- Lis�t��n turnauksiin testidataa, eli nimi ja kellonaika. Now()-funktio palauttaa nykyisen kellonajan
INSERT INTO tournament (tournament_name, tournament_time) VALUES ("Eka testiturnaus", NOW());
INSERT INTO tournament (tournament_name, tournament_time) VALUES ("Toka testiturnaus", NOW());

-- Lis�t��n kahteen edelliseen turnaukseen kaksi sarjaa kumpaankin
INSERT INTO league (tournament_id, league_name) VALUES (1,"Testisarja 1");
INSERT INTO league (tournament_id, league_name) VALUES (1,"Testisarja 2");
INSERT INTO league (tournament_id, league_name) VALUES (2,"Testisarja 3");
INSERT INTO league (tournament_id, league_name) VALUES (2,"Testisarja 4");

-- Lis�t��n asetuksiin erilaisia voittokertoimia
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);
INSERT INTO setting(setting_gameWinPt, setting_gameTiePt, setting_gameLosePt) VALUES (3,1,0);

-- Lis�t��n jokaiseen sarjaan kaksi testilohkoa, eli yhteens� kahdeksan testilohkoa.
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (1, "Testilohko 1", 1);
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (1, "Testilohko 2", 2);
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (2, "Testilohko 3", 3);
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (2, "Testilohko 4", 4);
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (3, "Testilohko 5", 5);
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (3, "Testilohko 6", 6);
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (4, "Testilohko 7", 7);
INSERT INTO division (league_id, division_name, division_setting_id) VALUES (4, "Testilohko 8", 8);

-- Lis�t��n jokaiseen lohkoon kaksi joukkuetta joilla on random-pisteit�, eli yhteens� 16 joukkuetta.
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (1, "Joukkue 1",2,3,1);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (1, "Joukkue 2",4,1,0);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (2, "Joukkue 3",2,0,2);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (2, "Joukkue 4",4,0,3);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (3, "Joukkue 5",1,1,5);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (3, "Joukkue 6",4,2,2);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (4, "Joukkue 7",5,0,3);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (4, "Joukkue 8",1,1,4);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (5, "Joukkue 9",2,2,5);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (5, "Joukkue 10",3,3,2);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (6, "Joukkue 11",3,2,3);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (6, "Joukkue 12",5,1,5);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (7, "Joukkue 13",6,3,2);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (7, "Joukkue 14",2,2,1);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (8, "Joukkue 15",3,1,1);
INSERT INTO team (division_id, team_name, team_win, team_tie, team_lose) VALUES (8, "Joukkue 16",5,1,2);

-- Lis�t��n v�h�n testipelej�. NOW()-funktio palauttaa luonnin hetkisen kellonajan
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (1,2,NOW(),1);
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (3,4,NOW(),2);
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (5,6,NOW(),3);
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (7,8,NOW(),4);
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (9,10,NOW(),5);
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (11,12,NOW(),6);
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (13,14,NOW(),7);
INSERT INTO game (game_team1, game_team2, game_time, field_id) VALUES (15,16,NOW(),8);

-- Lis�t��n pistetauluun testitietoja. score_part kuvastaa eri pelien puoliaikoja, jotta samalla ottelulla voi olla erikseen er�tuloksia
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (1,2,1,1);
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (3,4,1,2);
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (5,6,1,3);
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (7,8,1,4);
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (9,10,1,5);
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (9,10,1,6);
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (7,8,1,7);
INSERT INTO score(score_team1, score_team2,score_part,game_id) VALUES (5,6,1,8);

-- Lis�t��n admin tauluun yhden yll�pit�j�n tiedot. Admin_permissionniin tulee serialisoitu PHP-array jonka avulla voidaan autentikoida muokattavien turnausten oikeudet
INSERT INTO admin(admin_name, admin_firstName, admin_secondName, admin_password, admin_email, admin_permission) VALUES ("admin", "etunimi", "sukunimi", "8fe4c11451281c094a6578e6ddbf5eed", "admin@admin.com", "a:2:{i:0;i:1;i:1;i:3;}");


-- Palautetaan skriptin alussa laitetut flagit oletustilaan. Kanta testidatoineen on nyt valmis.
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
