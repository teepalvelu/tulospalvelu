<?php

	echo '<h2>Sarjataulukko</h2>';
	echo '<table class="table table-bordered">';
		echo '<tr><th>Joukkue</th><th>Voitot</th><th>Tasapelit</th><th>Tappiot</th><th>Pisteet</th></tr>';
		
		foreach($GLOBALS['database']->getSeries($command[1]) as $row) 
		{
			echo '<tr><td>'.$row["team_name"].'</td>
				<td>'.$row["team_win"].'</td>
				<td>'.$row["team_tie"].'</td>
				<td>'.$row["team_lose"].'</td>
				<td>'.$row["team_points"].'</td>
				</tr>';
		}
		
	echo '</table>';
	
	
	$divisioninfo = $GLOBALS['database']->getDivisionById($command[1])->fetch();
    $settings = $GLOBALS['database']->getSettings($divisioninfo["division_setting_id"])->fetch();
	echo '<h2>Ottelut</h2>';
	echo '<table class="table table-bordered">';
		echo '<tr><th>Aika</th><th>Kenttä</th><th>Koti</th><th>Vieras</th><th>Tulos</th></tr>';
		
		foreach($GLOBALS['database']->getGames($command[1]) as $row) 
		{

			echo '<tr><td>'.date('j.n.Y H:i',strtotime($row["game_time"])).'</td>
				<td>'.$row["field_name"].'</td>
				<td>'.$row["team1_name"].'</td>
				<td>'.$row["team2_name"].'</td><td>';
				for($i=1;$i<=$settings["setting_gameType"];$i++){
					$score = $GLOBALS['database']->getScore($row["game_id"], $i)->fetch();
						if($score["score_team1"] != null){echo $score["score_team1"];}else{echo 'x';}
						echo '-';
						if($score["score_team2"] != null){echo $score["score_team2"];}else{echo 'x';}
					if($i < $settings["setting_gameType"]){
						echo ', ';
					}
				}
				echo '</td></tr>';
		}
		
	echo '</table>';	
?>