﻿<?php	
	/********************************************************
	* Script: Fetch live games
	* Description: Waits for an Ajax-call to output 
				   newest game scores
	********************************************************/
    echo '<hr>';
	echo '<h2>Viimeisimmät päättyneet ottelut</h2>';
	echo '<table class="table table-bordered">';
		echo '<tr><th>Alkanut</th><th>Turnaus</th><th>Sarja</th><th>Lohko</th><th>Kenttä</th><th>Koti</th><th>Vieras</th><th>Tulos</th></tr>';
		
		foreach(getGames() as $row) 
		{
			
		    $scores = getScores($row["game_id"])->fetchall();
			end($scores);
			$lastkey = key($scores);
			echo '<tr><td>'.date("j.n.Y H:i",strtotime($row["game_time"])).'</td>';
			echo '<td>'.$row["tournament_name"].'</td><td>'.$row["league_name"].'</td><td>'.$row["division_name"].'</td>';
			echo '
				<td>'.$row["field_name"].'</td>
				<td>'.$row["team1_name"].'</td>
				<td>'.$row["team2_name"].'</td><td>';
				foreach($scores as $key => $score){
						if($score["score_team1"] != null){echo $score["score_team1"];}else{echo 'x';}
						echo '-';
						if($score["score_team2"] != null){echo $score["score_team2"];}else{echo 'x';}
					if($key != $lastkey)
					{
						echo ', ';
					}
				}
				echo '</td></tr>';
		}
	echo '</table>';	
	
	function getGames()
	{
	    include('../includes/settings.php');
		$games_limit = 8;
		
		try
		{
			$db = new PDO('mysql:host=' . $db_host . ';dbname=' . $db_name . ';charset=utf8', $db_user, $db_pass);
		} 
		catch(PDOException $e)
		{
			Die("Error");
		}
	
		$query = 'SELECT 
				game.game_id as game_id,
				game.game_time as game_time,
				team1.team_name as team1_name,
				team2.team_name as team2_name,
				field.field_name as field_name,
				division.division_name as division_name,
				league.league_name as league_name,
				tournament.tournament_name as tournament_name
			FROM
				game AS game
			LEFT JOIN
				team AS team1
				ON team1.team_id = game.game_team1
			LEFT JOIN
				team AS team2
				ON team2.team_id = game.game_team2
			LEFT JOIN
				division as division
				ON division.division_id = team1.division_id
			LEFT JOIN
				league as league
				ON league.league_id = division.league_id
			LEFT JOIN 
				tournament as tournament
				ON tournament.tournament_id = league.tournament_id
			LEFT JOIN
				field AS field
				ON game.field_id = field.field_id
			WHERE game.game_state = 1
			GROUP BY game.game_id
			ORDER BY game.game_timestamp DESC
			LIMIT ' . $games_limit;
			
		$pre = $db->prepare($query);
		$pre->execute();
		return $pre;
	}
	
	function getScores($gid)
	{
	    include('../includes/settings.php');
		
		try
		{
			$db = new PDO('mysql:host=' . $db_host . ';dbname=' . $db_name . ';charset=utf8', $db_user, $db_pass);
		} 
		catch(PDOException $e)
		{
			Die("Error");
		}
	
		$query = 'SELECT 
				score_team1 as score_team1,
				score_team2 as score_team2
			FROM
				score
			WHERE game_id = ?
			ORDER BY score_part ASC';
			
		$pre = $db->prepare($query);
		$pre->execute(array($gid));
		return $pre;
	}
	
?>	