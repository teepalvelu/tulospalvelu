<?php
	echo '<h2>Turnaukset</h2>';
		echo '<table class="table table-bordered">';
			echo '<tr><th>Nimi</th><th>Alkaa</th><th>Sarjoja</th></tr>';
		
			foreach($GLOBALS['database']->getTournaments() as $row) 
			{
				echo '<tr>';
					echo '
					<td><a href="'.getBasePath().'/tournament/'.$row["t_id"].'">'.$row["t_name"].'</a></td>
					<td>'.date("j.n.Y",strtotime($row["t_time"])).'</td>
					<td>'.$row["leagues"].'</td>';
				echo '</tr>';
			}
		echo '</table>';
?>