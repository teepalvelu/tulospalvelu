<?php
	/********************************************************
	* Script: Startup
	* Description: Includes core scripts and implements
				   new Database controller
	********************************************************/

	require_once ('./class/Settings.class.php');
	require_once ('./class/Errors.class.php');
	require_once ('./class/DatabaseController.class.php');
	require_once ('settings.php');

	Settings::setProtected('db_hostname', $db_host);
	Settings::setProtected('db_username', $db_user);
	Settings::setProtected('db_password', $db_pass);
	Settings::setProtected('db_database', $db_name);

	$GLOBALS['database'] = new DatabaseController;
?>