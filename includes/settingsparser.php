﻿<?php

class SettingsParser
{
	public $ini;
	
	function __construct()
	{
		$this->ini = $this->parseIni();
	}

	private function parseIni()
	{
		return parse_ini_file('settings.ini', true);
	}
}

?>