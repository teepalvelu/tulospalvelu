<?php
	/********************************************************
	* Script: Functions
	* Description: Core- and helper-functions
	********************************************************/

	function parseErrors($errorlist)
	{
		foreach($errorlist as $message)
		{
			echo '<p>'.$message.'</p>';
		}
	}

	function getBasePath()
	{
		$requestURI = explode("/", $_SERVER["REQUEST_URI"]);
		$scriptName = explode("/",$_SERVER["SCRIPT_NAME"]);
		$basepath = "";
		
		for($i= 0; $i < sizeof($scriptName); $i++)
		{
			if ($requestURI[$i] == $scriptName[$i] && $requestURI[$i] != "")
			{
				$basepath .= "/" . $scriptName[$i];
			}
		}
		return $basepath;
	}

	function getCommands()
	{
		$requestURI = explode("/", $_SERVER["REQUEST_URI"]);
		$scriptName = explode("/",$_SERVER["SCRIPT_NAME"]);
		 
		for($i= 0; $i < sizeof($scriptName); $i++)
		{
			if ($requestURI[$i] == $scriptName[$i])
			{
				unset($requestURI[$i]);
			}
		}
		
		return array_values($requestURI);
	}

	function showContent()
	{
		$command = getCommands();

		if($command[0] == "")
		{
			//$command[0] = "tournamentlist";
			//Joo eli nyt se tyhj� command on mahoton kun me redirectataan
			//suoraan tohon tournamentlistiin
			header('Location: ' . getBasePath() . '/tournamentlist/'); 
																		
		}
		
		include('view/'.$command[0].'.php');
	}
	
	function resolver($type, $id){
		switch($type){
			case "league":
				foreach ($GLOBALS["database"]->leagueResolver($id) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]),
							"league" =>  array("id" => $data["league_id"], "name" => $data["league_name"]));
				}
				break;
			case "division":
				foreach ($GLOBALS["database"]->divisionResolver($id) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]),
							"league" =>  array("id" => $data["league_id"], "name" => $data["league_name"]),
							"division" =>  array("id" => $data["division_id"], "name" => $data["division_name"]));
				}
			break;
			case "tournament":
				foreach ($GLOBALS["database"]->tournamentResolver($id) as $data){
				return array("tournament" => array("id" => $data["tournament_id"], "name" => $data["tournament_name"]));
				}
			break;
		}
	}
?>