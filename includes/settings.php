<?php
	/********************************************************
	* File: Database settings
	* Description: Contains connection info for the database
	********************************************************/
	
	require_once('settingsparser.php');
	$s = new SettingsParser;
	
	$database = $s->ini['DATABASE'];
	
	$db_user = $database['db_user'];
	$db_pass = $database['db_pass'];
	$db_host = $database['db_host'];
	$db_name = $database['db_name'];
?>